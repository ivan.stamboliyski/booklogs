BEGIN TRANSACTION;

CREATE TABLE books ( 
	id                   integer PRIMARY KEY,
	author               text     ,
	title                text     ,
	cover                text     ,
	status               text DEFAULT 'available' CHECK( status IN ('available','borrowed','unlisted') )     ,
  user_id              integer  ,
	deleted              integer  DEFAULT 0   CHECK( deleted in (0,1) ) ,
  FOREIGN KEY ( user_id ) references users( id )
 );

CREATE TABLE users ( 
	id                   integer PRIMARY KEY AUTOINCREMENT,
	username             text NOT NULL,
	password             text NOT NULL,
  avatar               text,
	reading_points       integer  DEFAULT 0,
	role                 text DEFAULT 'user'   CHECK( role in ('user','admin') )     ,
	deleted              integer  DEFAULT 0   CHECK( deleted in (0,1) )
 );

CREATE TABLE banstatus ( 
	user_id              integer NOT NULL  PRIMARY KEY  ,
	banned               integer  DEFAULT 1   ,
	description          text     ,
	expiration           integer     ,
	FOREIGN KEY ( user_id ) REFERENCES users( id )  
 );

CREATE TABLE ratings ( 
	user_id              integer NOT NULL    ,
	book_id              integer NOT NULL    ,
	value                integer     ,
  return_date          integer     ,
	deleted              integer  DEFAULT 0   CHECK( deleted in (0,1) ) ,
	CONSTRAINT Pk_ratings PRIMARY KEY ( user_id, book_id ),
	FOREIGN KEY ( book_id ) REFERENCES books( id )  ,
	FOREIGN KEY ( user_id ) REFERENCES users( id )  
 );

CREATE TABLE review_likes ( 
	user_id              integer NOT NULL    ,
	reviewer_id          integer NOT NULL    ,
	book_id              integer NOT NULL    ,
	deleted              integer  DEFAULT 0   CHECK( deleted in (0,1) ) ,
	CONSTRAINT Pk_review_likes PRIMARY KEY ( user_id, reviewer_id, book_id ),
	FOREIGN KEY ( user_id ) REFERENCES users( id )  ,
	FOREIGN KEY ( reviewer_id ) REFERENCES users( id )   ,
	FOREIGN KEY ( book_id ) REFERENCES books( id )  
 );

CREATE TABLE reviews ( 
	user_id              integer NOT NULL    ,
	book_id              integer NOT NULL    ,
	title                text NOT NULL    ,
	content              text NOT NULL    ,
	deleted              integer  DEFAULT 0   CHECK( deleted in (0,1) ) ,
  -- id                   text GENERATED ALWAYS AS (user_id || book_id)    ,
	CONSTRAINT Pk_reviews PRIMARY KEY ( user_id, book_id ),
	FOREIGN KEY ( user_id ) REFERENCES users( id ),
	FOREIGN KEY ( book_id ) REFERENCES books( id )  
 );

CREATE TABLE tokens (
  token                 text NOT NULL  PRIMARY KEY
);

 COMMIT;