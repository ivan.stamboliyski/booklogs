# Library Server

Server app for the Library Web project

[TOC]



## Set up

### Prerequisites

- Node.js v14.8+
- npm v7+

### Initialization and starting

1. `npm install` install dependencies
2. `npm run init` recreate the database schema and fill in initial data
3. `npm start` start the server 
4. Make requests via Postman or your Web app.

### Working with the Postman collection

A collection of predefined requests is available to help you practice retrieving data from the server. You can work with the collection by importing it in [Postman](postman.com).

**Precondition**: Server data is initialized and server is **started** (see above).

- Import `postman/LibraryTA.postman_collection.json` to Postman. [How to import](https://letmegooglethat.com/?q=how+to+import+collection+in+postman)
- Execute requests, modify request route or body whenever needed.
- The user `/register` and `/login` routes do not require authentication. All other routes expect requests with an Authorization header, Bearer token.

Authentication **option 1 user**: Register a user, log them in, store their token.

- Run the *user register* request to create a new user (first update body with your new user username and password).
- Run the *user login* request to get a JWT token (first update body with your username and password). Token expiration is set to 5 days, which means, unless you log out, you can use this token for all other requests. When the token expires, you will need to log in again. You can check expiration with [jwt.io](https://jwt.io/).
- Copy the value of the token from the response of the *user login* request .
- Create environment variable named `token` and store the token value under `current value`  (how to [manage environments](https://learning.postman.com/docs/sending-requests/managing-environments/)). Requests which require authentication are configured to look for an environment variable named `token` and set it in the Authorization header. 
- If you log out and log in again the environment variable `token` needs to be updated with the new token returned from the login request.

Authentication **option 2 admin**: Use pre-defined active tokens from the database.

- To access the admin routes, there are two users who are initialized with the DB as logged in. Use one of their tokens (line 27 and 28 of the `init-data.sql` file) 
- Store the token as an environment variable as described above. 
- Then try to update your user to have an admin role.

**Troubleshooting** Postman responses

- Error: connect ECONNREFUSED 127.0.0.1:5000 -- is the server running?
- 401Unauthorized -- you are calling an endpoint which requires authentication
- 403 Forbidden -- you are authenticated but the user does not have access to this resource
- Other error codes are listed at the end of this document



----

## Type Definitions

### User

```typescript
type User = {
    id: number,
    username: string,
    password?: string, //hash
    role?: "user"|"admin",
    avatar: string,
    banstatus?: Banstatus
}
```

### Book

```typescript
type Book = {
    id: number,
    title: string,
    author: string,
    cover: string,
    status: "available"|"borrowed"|"unlisted",
    userId: number|null, // the user who has borrowed the book atm
    rating?: number|null,
    reviews?: Review[]
}
```

```typescript
type Pagination = {
    totalCount: number,
    count: number,
    offset: number
}
```



### Review

Review unique identifier is the combination of `userId` and `bookId`. 

```typescript
type Review = {
    title: string,
    content: string,
    userId: number,
    username: string,
    avatar: string,
    bookId: number,
    bookTitle: string,
    cover: string,
    votes?: [{
        userId: number,
        username: string
    }]
    // additional properties
}
```

### Banstatus

```typescript
type Banstatus = {
    userId: number,
    banned: 0|1,
    description: string,
    expiration: number // milliseconds from 1.1.1970
}
```

----

## Public resources

### Register

✅ `POST /api/v1/users`

Required *username* (3-25 characters) and *password* (3-25 characters) fields. Two users with the same username cannot exist. Password hash is stored.  Accepts json or form-data but json can not upload avatar file.

Example request body (json):

```json
{
    "username": "maria",
    "password": "123"
}
```

Example request body (form-data):

❗❗ Use `FormData` to upload avatar files. (how to work with [form-data in Postman](https://learning.postman.com/docs/sending-requests/requests/#form-data), [FormData in JS](https://javascript.info/formdata))

Image files uploaded for user avatar and book covers, can be retrieved at `[api-url]/covers/[img-name]`  for example http://localhost:5000/covers/1634591356102.jpg

| key      | value              |
| -------- | ------------------ |
| username | maria              |
| password | 123                |
| avatar   | upload avatar file |

Sample response status 201, body: `User`, or an error message

```json
{
    "id": 6,
    "username": "maria",
    "avatar": "1634595915045.png"
}
```



### Login

✅ `POST /api/v1/users/login`

Required fields: *username* and *password*. Checks if user with such username exists and if so, checks if the password from the request body matches the password in the database. If checks pass, responds with a JWT token, to be used to authenticate the user.

Example request body:

```json
{
    "username": "maria",
    "password": "123"
}
```

Example response body:

```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoibmFkaWEiLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2MzQyNDk3NDcsImV4cCI6MTcyMDY0OTc0N30.bvYYr7P2JJc7pDQYu_AThql9kYIznNifywbvwzEORHM"
}
```



---

## Private resources

Accessible to logged in users.

### Get user info

✅ `GET /api/v1/users/:id`

Retrieves user information. Useful when needing to update views showing points.

Example request body: none

Example response body: `User`

```json
{
    "id": 3,
    "username": "trinity",
    "role": "user",
    "avatar": "trinity-avatar.png",
    "points": 133,
    "banstatus": null
}
```

### Get all users info

✅ `GET /api/v1/users`

Retrieves user information about all users.

Example request body: none

Example response body: `User[]`

```json
{
    "id": 3,
    "username": "trinity",
    "role": "user",
    "avatar": "trinity-avatar.png",
    "points": 133,
    "banstatus": null
}
```

### Update user

✅ `PUT /api/v1/users`

A logged in user is able to modify their username, password or avatar. Validated the same as when creating a user but not all properties are required (can send only the updated properties). If successful, the user is logged out. Accepts json or form-data.

Example request body: ❗❗ Use `FormData` to upload avatar files. (how to work with [form-data in Postman](https://learning.postman.com/docs/sending-requests/requests/#form-data), [FormData in JS](https://javascript.info/formdata))

| key      | value              |
| -------- | ------------------ |
| username | maria              |
| avatar   | upload avatar file |

Example response body:

```json
{
    "message": "Successfully updated user info, you need to log in again",
    "user": {
        "id": 5,
        "username": "nadia",
        "role": "user",
        "avatar": "1634816939374.png",
        "deleted": 0,
        "banstatus": null
    }
}
```

### Logout

✅ `POST /api/v1/users/logout`

Allows a user to log out. Listens for an **authenticated** request with a valid bearer token and invalidates that token.

Example request body: none

Example response body: success or error message

```json
{
    "message": "Log out success"
}
```

```json
{
    "message": "User not signed in"
}
```

### Retrieve all books

✅ `GET /api/v1/books`

A user can retrieve all the books in the library. Each book has a status that indicates if the book is currently `borrowed`, if the book is `unlisted` (cannot be retrieved or borrowed) and if the book if `available` and can be borrowed. 

Example request body: none

Example response body: `{ books: Book[], pagination: Pagination }`

```json
{
    "books": [
        {
            "id": 6,
            "title": "Brave New World",
            "author": "Aldous Huxley",
            "status": "available",
            "cover": "brave-new-world.jpg",
            "rating": null
        },
        {
            "id": 1,
            "title": "The Art of Loving",
            "author": "Erich From",
            "status": "unlisted",
            "cover": "art-of-loving.jpg",
            "rating": 3.33
        }
    ],
    "pagination": {
        "totalCount": 7,
        "count": 2,
        "offset": 0
    }
}
```

#### Searching for a book

✅ `GET /api/v1/books?author=george&status=available`

Books can be filtered by properties `id`, `author`, `title`, `status`. When using multiple query parameters, the search produces results for all of them (using AND check).

#### Pagination

✅ `GET /api/v1/books?offset=5&count=10&order_by=id&order_dir=DESC`

Query parameters can be used for pagination. Default values are used if the parameters are missing.

| Key       | Description                                       | Possible values                   | Default value |
| --------- | ------------------------------------------------- | --------------------------------- | ------------- |
| offset    | Defines how many items to skip from the beginning | 0 to a reasonable number          | 0             |
| count     | The number of elements to return                  | 0 to a reasonable number          | 20            |
| order_by  | The column to order by                            | id, title, author, status, rating | id            |
| order_dir | The direction of sorting                          | ASC, DESC                         | ASC           |

Pagination information returned: 

- count - the count of elements returned. If the count is less than requested, you are on the last page.
- total count - total count of elements in the table (might include deleted books)
- offset - the number of items skipped

Example use: If you have 5 items per page and want to get the books for the third page: `?offset=10&count=5` - skip the first two pages of 5 books each, get the next 5 books.

### View individual book

✅ `GET /api/v1/books/:id`

A user can retrieve an individual book with all reviews it has and the average of all ratings users gave.

Example request body:  none

Example response body: `Book`

```json
{
    "id": 3,
    "title": "1984",
    "author": "George Orwell",
    "status": "borrowed",
    "cover": "1984.jpg",
    "userId": 2,
    "rating": 2,
    "reviews": [
        {
            "title": "A must read",
            "content": "Definitely reader's choice!",
            "userId": 1,
            "username": "neo",
            "avatar": "neo-avatar.png",
            "bookId": 3,
            "bookTitle": "1984",
            "cover": "1984.jpg",
            "votes": [
                {
                    "userId": 1,
                    "username": "neo",
                    "avatar": "neo-avatar.png"
                },
                {
                    "userId": 2,
                    "username": "morpheus",
                    "avatar": "morpheus-avatar.png"
                }
            ]
        },
        {
            "title": "A must!",
            "content": "Definitely reader's choice!!",
            "userId": 2,
            "username": "morpheus",
            "avatar": "morpheus-avatar.png",
            "bookId": 3,
            "bookTitle": "1984",
            "cover": "1984.jpg",
            "votes": [
                {
                    "userId": 1,
                    "username": "neo",
                    "avatar": "neo-avatar.png"
                }
            ]
        }
    ]
}
```



### Borrow a book

✅  `POST /api/v1/books/:id`

A user can borrow a book, if it is available. Checks if it can be borrowed and updates its status. 

Example request body: none

Example response body: `Book`

```json
{
    "id": 5,
    "title": "The Catcher in the Rye",
    "author": "J. D. Salinger",
    "status": "borrowed",
    "cover": "the-catcher-in-the-rye.jpg",
    "userId": 1
}
```



### Return a book

✅ `DELETE /api/v1/books/:id`

A user can return a book they have borrowed. Users can return only books they have borrowed.

Example request body: none

Example response body: `Book`

```json
{
    "id": 5,
    "title": "The Catcher in the Rye",
    "author": "J. D. Salinger",
    "status": "available",
    "cover": "the-catcher-in-the-rye.jpg",
    "userId": null
}
```



### Read book reviews

✅ `GET /api/v1/books/:id/reviews`

Retrieves all reviews for the book id specified (with votes).

Example request body: none

Example responses: `Review[]`

```json
[
    {
        "title": "title3",
        "content": "My favourite",
        "userId": 1,
        "username": "neo",
        "avatar": "neo-avatar.png",
        "bookId": 3,
        "bookTitle": "1984",
        "cover": "1984.jpg",
        "votes": [
            {
                "userId": 1,
                "username": "neo",
                "avatar": "neo-avatar.png"
            },
            {
                "userId": 2,
                "username": "morpheus",
                "avatar": "morpheus-avatar.png"
            }
        ]
    }
]
```



### Read user reviews

✅ `GET /api/v1/users/:id/reviews`

Retrieves all reviews for the user id specified (with votes).

Example request body: none

Example responses: `Review[]`

```json
[
    {
        "title": "All time favorite book",
        "content": "If you read one book in your life, let it be this one",
        "userId": 1,
        "username": "neo",
        "avatar": "neo-avatar.png",
        "bookId": 2,
        "bookTitle": "Fahrenheit 451",
        "cover": "fahrenheit-451.jpg",
        "votes": []
    },
    {
        "title": "title3",
        "content": "My favourite",
        "userId": 1,
        "username": "neo",
        "avatar": "neo-avatar.png",
        "bookId": 3,
        "bookTitle": "1984",
        "cover": "1984.jpg",
        "votes": [
            {
                "userId": 1,
                "username": "neo",
                "avatar": "neo-avatar.png"
            },
            {
                "userId": 2,
                "username": "morpheus",
                "avatar": "morpheus-avatar.png"
            }
        ]
    }
]
```



### Create book review

✅ `POST /api/v1/books/:id/reviews`

Required fields *title* (5-50 characters), *content* (10-500 characters). For the authenticated user. Only one per user per book possible.

Example request body: 

```json
{
    "title": "Uber great",
    "content": "I still think it's great!"
}
```

Example response body: `Review` or an error message

```json
{
    "title": "Uber great",
    "content": "I still think it's great!",
    "userId": 1,
    "username": "neo",
    "avatar": "neo-avatar.png",
    "bookId": 6,
    "bookTitle": "Brave New World",
    "cover": "brave-new-world.jpg",
    "votes": []
}
```

```json
{
    "message": "Only one review per user per book is possible"
}
```



### Update book review

✅ `PUT /api/v1/books/:id/reviews`

Allows the user to edit a review for the book id specified. The user can edit **only their own reviews**. Validations:  *title* (5-50 characters), *content* (10-500 characters). Possible to update individual fields (send only updated properties).

Example request body: 

```json
{
    "title": "A must read",
    "content": "Definitely read this book!"
}
```

Example request body: 

```json
{
    "content": "Put this book in your to-read list"
}
```

Example response body: Updated `Review` or an error message

```json
{
    "title": "A must read",
    "content": "Definitely reader's choice!",
    "userId": 1,
    "username": "neo",
    "avatar": "neo-avatar.png",
    "bookId": 3,
    "bookTitle": "1984",
    "cover": "1984.jpg",
    "votes": [
        {
            "userId": 1,
            "username": "neo",
            "avatar": "neo-avatar.png"
        },
        {
            "userId": 2,
            "username": "morpheus",
            "avatar": "morpheus-avatar.png"
        }
    ]
}
```



### Delete book review

✅ `DELETE /api/v1/books/:id/reviews`

Allows a user to remove their book review for the book id specified. The user can remove **only their own reviews**. Does not actually delete, instead uses `deleted` flag. 

Example request body: none

Example response body: The deleted `Review` or an error message

```json
{
    "title": "A must read",
    "content": "Definitely reader's choice!",
    "userId": 1,
    "username": "neo",
    "avatar": "neo-avatar.png",
    "bookId": 3,
    "bookTitle": "1984",
    "cover": "1984.jpg"
}
```



### Rate book

✅ `PUT /api/v1/books/:id/rate`

A user can rate a book only if they have **borrowed, returned and reviewed it**. Rating is a number between 1 and 5. Once set, the rating can only be updated with a different number value.

Example request body: 

```json
{
    "value": 5
}
```

Example responses: The book’s data or an error message.

```json
{
    "id": 3,
    "title": "1984",
    "author": "George Orwell",
    "status": "available",
    "cover": "1984.jpg",
    "userId": null,
    "rating": 3.5
}
```



### Like reviews

✅ `PUT /api/v1/books/:id/reviews/:reviewerId`

The user is able to like/unlike a review. The reviews have additional property *votes*.  Sending a second request to the same endpoint, removes the like.

Example request body: none

Example responses: `Review` or an error message

```json
{
    "title": "A must read",
    "content": "Definitely reader's choice!",
    "userId": 1,
    "username": "neo",
    "avatar": "neo-avatar.png",
    "bookId": 3,
    "bookTitle": "1984",
    "cover": "1984.jpg",
    "votes": [
        {
            "userId": 2,
            "username": "morpheus",
            "avatar": "morpheus-avatar.png"
        }
    ]
}
```



----

## Administration resources

Accessible only to users with role `admin`. They can **create, read, update, and delete** any book or review. 

###  CRUD books

✅ `GET /api/v1/admin/books/` - Retrieves all books, including the deleted and unlisted ones. Allows querying by book properties. To query deleted books, add `?deleted=0` query parameter. Pagination works the same as with the non-admin route.

✅ `POST /api/v1/admin/books/` - Add a new book - *title* (5-50 characters), *author* (5-50 characters). Accepts json or form-data but json can not upload book cover file.

```json
{
    "title": "Catch-22",
    "author": "Joseph Heller",
    "status": "borrowed"
}
```



Example request body: ❗❗ Use `FormData` to upload book cover files. (how to work with [form-data in Postman](https://learning.postman.com/docs/sending-requests/requests/#form-data), [FormData in JS](https://javascript.info/formdata))

Files uploaded for user avatar and book covers, can be accessed at `[api-url]/covers/[img-name]`  for example http://localhost:5000/covers/1634591356102.jpg

| key    | value                  |
| ------ | ---------------------- |
| title  | Catch-22               |
| author | Joseph Heller          |
| cover  | upload book cover file |

Example response body: `Book`

```json
{
    "id": 8,
    "title": "Catch-22",
    "author": "Joseph Heller",
    "cover": "catch-22.jpg",
    "status": "available"
}
```



✅ `PUT /api/v1/admin/books/:id` - Update books. Will update only changed fields. Possible to update a book's deleted status from 1 to 0 (un-delete a book). Fields are validated. Possible to update individual fields (no need to send all book properties). Accepts json or form-data.

Example request body: ❗❗ Use `FormData` to upload book cover files. (how to work with [form-data in Postman](https://learning.postman.com/docs/sending-requests/requests/#form-data), [FormData in JS](https://javascript.info/formdata))

| key    | value                  |
| ------ | ---------------------- |
| title  | Catch-22               |
| author | Joseph Heller          |
| cover  | upload book cover file |



✅ `DELETE /api/v1/admin/books/:id` - Delete a book.

Example request body: none

Example response body: `Book`

```json
{
    "id": 7,
    "title": "Into the Wild",
    "author": "Jon Krakauer",
    "status": "available",
    "cover": "into-the-wild.jpg",
    "userId": null,
    "deleted": 1
}
```



### CRUD reviews

✅ GET `api/v1/admin/reviews` - Retrieve all reviews for all books and all users (without the deleted ones).

Example response body:

```json
[
    {
        "title": "title1",
        "content": "Greatest book ever",
        "userId": 1,
        "username": "neo",
        "avatar": "neo-avatar.png",
        "bookId": 1,
        "bookTitle": "The Art of Loving",
        "cover": "art-of-loving.jpg"
    },
    {
        "title": "All time favorite book",
        "content": "If you read one book in your life, let it be this one",
        "userId": 1,
        "username": "neo",
        "avatar": "neo-avatar.png",
        "bookId": 2,
        "bookTitle": "Fahrenheit 451",
        "cover": "fahrenheit-451.jpg"
    }
 ]
```

✅ POST `api/v1/admin/books/:id/reviews/:userId`  - Add a review for a specific user (different from the currently logged in).

✅ PUT `api/v1/admin/books/:id/reviews/:userId` - Update a user's review (different from the currently logged in).

✅ DELETE `api/v1/admin/books/:id/reviews/:userId` - Delete a user's review (different from the currently logged in).

### Retrieve all users

✅ `GET /api/v1/admin/users/`

Admins are able to get a list of all users with their banstatus.

Example request body: none

Example response body: `User[]`

```json
[
    {
        "id": 1,
        "username": "neo",
        "role": "admin",
        "deleted": 0,
        "banstatus": {
            "userId": 1,
            "banned": 1,
            "description": "Too many late returns",
            "expiration": 1633043760304
        }
    },
    {
        "id": 2,
        "username": "morpheus",
        "role": "admin",
        "deleted": 0,
        "banstatus": {
            "userId": 2,
            "banned": 0,
            "description": "Mean reviews",
            "expiration": 1633043666171
        }
    }
]
```

### Update users

✅ `PUT /api/v1/admin/users/:id`

Allows admin users to change user roles, restore deleted users, and update any user properties. Accepts json or form-data.

Example request body: ❗❗ Use `FormData` to upload avatar files. (how to work with [form-data in Postman](https://learning.postman.com/docs/sending-requests/requests/#form-data), [FormData in JS](https://javascript.info/formdata))

Example request body: 

```json
{
    "role": "admin",
    "deleted": 0
}
```

Example response body:

```json
{
    "message": "Successfully updated user info, if role was changed, they need to log in again",
    "user": {
        "id": 5,
        "username": "nadia",
        "role": "user",
        "avatar": "1634816939374.png",
        "deleted": 0,
        "banstatus": null
    }
}
```

### Delete users

✅ `DELETE /api/v1/admin/users/:id`

An admin user can delete users from the library system. A new user with the same username as a deleted user cannot be registered. However, admins can un-delete users through the update endpoint by setting the deleted property to 0.

Example request body: none

Example responses: The deleted user’s data or an error message

```json
{
    "id": 1,
    "username": "nadia",
    "role": "admin",
    "deleted": 1
}
```

### Ban users

✅ `PUT /api/admin/users/:id/ban` 

An admin user is able to ban and un-ban users (including admins). Required fields: *description* (5-50 characters), *expiration* (date in the future), *banned* (0 or 1). A ban has an expiration date (recorded in milliseconds from 1.1.1970). A banned user is restricted from every operation in the library (borrowing books, writing reviews, etc.), except reading. 

Example request body: 

```json
{
    "description": "They need a break",
    "expiration": 1638223200000,
    "banned": 1
}
```

Example responses: `User` or an error message

```json
{
    "id": 3,
    "username": "trinity",
    "role": "user",
    "deleted": 0,
    "banstatus": {
        "description": "They need a break",
        "expiration": 1638223200000,
        "banned": 1
    }
}
```

## Reading points / Gamification

🏆 Users get reading points:

- 5 for every book they have borrowed **and** returned
- 10 for writing a review 
- 2 or for having a review liked (-2 when unliked)
- and lose some percentage of points if banned, depending on how long they are banned for.

## Error codes

| Error code | Message                                                      |
| ---------- | ------------------------------------------------------------ |
| 400        | username/password mismatch                                   |
|            | User not signed in                                           |
|            | Data was not updated                                         |
| 403        | Operation not permitted                                      |
|            | Rating is only possible if book was borrowed, returned, and reviewed |
| 404        | Record does not exist or is deleted                          |
| 409        | The requirements allow only one of that resource             |
| 422        | The provided update value is the same as the stored one      |
|            | The provided data is invalid                                 |
| 500        | Error retrieving/saving data                                 |
|            | Unknown error                                                |

----

## Exploring the database

### SQLite DB manipulation (without JavaScript)

The db file is created by the init script in folder `./database/librarita.db`. Here are several options for viewing the data stored in the database:

1. [DB Browser for SQLite](https://sqlitebrowser.org/) - visual, open source tool to create, design, and edit database files compatible with SQLite. [Documentation](https://github.com/sqlitebrowser/sqlitebrowser/wiki)
2. [VSCode-SQLite extension](https://marketplace.visualstudio.com/items?itemName=alexcvzz.vscode-sqlite) - VSCode extension to explore and query SQLite databases. (does not work if the db has generated columns)
3. [DbSchema](https://dbschema.com/database-designer/Sqlite.html) - a SQLite GUI tool featuring interactive diagrams for designing SQLite databases.
4. [CLI](https://www.sqlite.org/cli.html) - The SQLite project provides a simple command-line program named **sqlite3** (or **sqlite3.exe** on Windows) that allows the user to manually enter and execute SQL statements against an SQLite database.

### Database schema

![schema-2](imgs/schema.jpg)

### Application function call flow

https://excalidraw.com/#json=5025607574355968,AG1O2pNPdEoMC7QVUnqPxg

![Function call flow](imgs/function-call-flow.png)

----

## Additional Info

### Debugging the server

Instead of `npm start` use keyboard key F5 (or menu Run > Start debugging) to start the server in debug mode. This allows adding breakpoints and exploring data.

### About SQLite

- [SQLite Tutorial](https://www.sqlitetutorial.net/)
- [Data Types](https://www.sqlitetutorial.net/sqlite-data-types/)
- [Concat](https://www.sqlitetutorial.net/sqlite-string-functions/sqlite-concat/)
- [Generated columns](https://www.sqlite.org/gencol.html)
- [Running SQL Queries from a ‘.sql’ file in NodeJS (SQLite)](https://levelup.gitconnected.com/running-sql-queries-from-an-sql-file-in-a-nodejs-app-sqlite-a927f0e8a545)

### SQLite3 Node

- [SQLite3 NPM](https://www.npmjs.com/package/sqlite3)
- [SQLite3 Documentation](https://github.com/mapbox/node-sqlite3/wiki)
