import { dbErrors, serviceErrors } from '../constants/constants.js';
import * as reviewData from '../data/reviewData.js';
import * as reviewLikesData from '../data/reviewLikesData.js';
import { getAllWithVotes, getError, getSuccess, getUpdatedProps, getWithVotes } from './common.js';
import * as points from './pointsService.js';

const updateExisting = async (userId, bookId, updates, reviewStored) => {

  const filtered = getUpdatedProps(reviewStored, updates);

  if (Object.keys(filtered).length > 0) {
    const updatesCount = await reviewData.update(userId, bookId, updates);
    delete reviewStored.deleted;
    const updated = await getWithVotes({ ...reviewStored, ...filtered });

    return updatesCount > 0 ?
      getSuccess(updated) :
      getError({ message: serviceErrors.NO_DATA_UPDATED });
  }

  return getError(serviceErrors.NOTHING_TO_UPDATE);
};

export const create = async (userId, bookId, draft) => {
  try {
    const reviewStored = await reviewData.getWithDeleted(userId, bookId);

    if (reviewStored) {
      // Creating a new review after an old one is deleted
      return reviewStored.deleted ?
        updateExisting(userId, bookId, draft, reviewStored) :
        getError(serviceErrors.DUPLICATE_RECORD);
    }

    const createdCount = await reviewData.create(userId, bookId, draft);

    if (createdCount > 0) {
      // get the new review with all its properties
      const review = await reviewData.getSingle(userId, bookId);
      await points.awardReviewing(userId);

      return getSuccess({ ...review, votes: [] });
    }

    return getError(serviceErrors.NO_DATA_UPDATED);
  } catch (error) {
    if (error === dbErrors.SQLITE_CONSTRAINT) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const getAll = async () => {
  try {
    const reviews = await reviewData.getAll();

    return getSuccess(reviews);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const getByBookId = async (id) => {
  try {
    const reviews = await reviewData.getByBookId(id);
    const reviewsWithVotes = await getAllWithVotes(reviews);

    return getSuccess(reviewsWithVotes);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const getByUserId = async (id) => {
  try {
    const reviews = await reviewData.getByUserId(id);
    const reviewsWithVotes = await getAllWithVotes(reviews);

    return getSuccess(reviewsWithVotes);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const update = async (userId, bookId, updates) => {
  try {
    const reviewStored = await reviewData.getSingle(userId, bookId);

    if (!reviewStored) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    return updateExisting(userId, bookId, updates, reviewStored);

  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const remove = async (userId, bookId) => {
  try {
    const review = await reviewData.getSingle(userId, bookId);

    if (!review) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    const updatesCount = await reviewData.remove(userId, bookId);

    if (updatesCount > 0) {
      return getSuccess(review);
    }

    return getError(serviceErrors.NO_DATA_UPDATED);
  } catch {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const toggleLike = async (userId, reviewerId, bookId) => {
  try {
    const review = await reviewData.getSingle(reviewerId, bookId);

    if (!review) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    const like = await reviewLikesData.get(userId, reviewerId, bookId);

    if (like) {
      const toggle = like.deleted ? 0 : 1;
      await reviewLikesData.update(userId, reviewerId, bookId, toggle);
      toggle ? await points.deductUnlike(reviewerId) : await points.awardLike(reviewerId);
    } else {
      await reviewLikesData.add(userId, reviewerId, bookId);
      await points.awardLike(reviewerId);
    }
    const reviewWithVotes = await getWithVotes(review);

    return getSuccess(reviewWithVotes);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};
