import bcrypt from 'bcrypt';
import { createToken } from '../authentication/createToken.js';
import { serviceErrors } from '../constants/constants.js';
import * as userData from '../data/userData.js';
import * as tokenData from '../data/tokenData.js';
import * as banData from '../data/banData.js';
import * as pointsService from './pointsService.js';
import { getError, getSuccess, getUpdatedProps } from './common.js';

export const create = async (username, password, avatar) => {
  try {
    const existingUser = await userData.getByWithPassword('username', username);

    if (existingUser) {
      return getError(serviceErrors.DUPLICATE_RECORD);
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const userId = await userData.create(username, passwordHash, avatar);

    return getSuccess({ id: userId, username, avatar });
  } catch (error) {
    return getError(serviceErrors.INVALID_DATA);
  }
};

export const get = async () => {
  try {
    const users = await userData.get();


    return getSuccess(users);

  } catch (error) {
    return getError(serviceErrors.INVALID_DATA);
  }
};

export const getUser = async (id) => {
  try {
    const user = await userData.getBy('id', id);

    if (user) {
      user.banstatus = await banData.get(user.id);

      return getSuccess(user);
    }

    return getError(serviceErrors.RECORD_NOT_FOUND);
  } catch (error) {
    return getError(serviceErrors.INVALID_DATA);
  }
};

export const getAll = async () => {
  try {
    const users = await userData.getAll();
    await Promise.all(users.map(async (user) => {
      user.banstatus = await banData.get(user.id);
    }));

    return getSuccess(users);
  } catch (error) {
    return getError(serviceErrors.INVALID_DATA);
  }
};

export const logIn = async (username, password) => {
  try {
    const user = await userData.getByWithPassword('username', username);
    if (!user || user.deleted) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    const passMatch = await bcrypt.compare(password, user.password);

    if (!passMatch) {
      return getError(serviceErrors.INVALID_SIGN_IN);
    } else {
      const payload = {
        sub: user.id,
        username: user.username,
        role: user.role,
      };
      const token = createToken(payload);
      await tokenData.add(token);

      return getSuccess(token);
    }
  } catch (error) {
    return getError(serviceErrors.INVALID_DATA);
  }
};

export const logOut = async (token) => {
  try {
    if (await tokenData.remove(token)) {
      return getSuccess({ message: 'Log out success' });
    }
    return getError(serviceErrors.INVALID_SIGN_OUT);
  } catch (err) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const update = async (userId, updates) => {
  try {
    const userStored = await userData.getByWithPassword('id', userId);

    if (!userStored) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    if (updates.password) {
      if (await bcrypt.compare(updates.password, userStored.password)) {
        delete updates.password;
      } else {
        updates.password = await bcrypt.hash(updates.password, 10);
      }
    }

    const filtered = getUpdatedProps(userStored, updates);

    if (Object.keys(filtered).length > 0) {
      if (updates.username) {
        const existingUser = await userData.getBy('username', updates.username);
        if (existingUser) {
          return getError(serviceErrors.DUPLICATE_RECORD);
        }
      }

      if (await userData.update(userId, filtered)) {
        const banstatus = await banData.get(userId);
        return getSuccess({
          id: userStored.id,
          username: filtered.username || userStored.username,
          role: filtered.role || userStored.role,
          avatar: filtered.avatar || userStored.avatar,
          deleted: filtered.deleted || userStored.deleted,
          banstatus,
        });
      }

      return getError(serviceErrors.NO_DATA_UPDATED);
    }

    return getError(serviceErrors.NOTHING_TO_UPDATE);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const remove = async (userId) => {
  try {
    const userStored = await userData.getBy('id', userId);

    if (!userStored) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    if (await userData.remove(userId)) {
      return getSuccess({ ...userStored, deleted: 1 });
    }

    return getError(serviceErrors.NO_DATA_UPDATED);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};


export const banUser = async (userId, details) => {
  try {
    const user = await userData.getBy('id', userId);
    if (!user) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    const banStored = await banData.get(userId);

    if (banStored) {
      const filtered = getUpdatedProps(banStored, details);
      if (Object.keys(filtered).length === 0) {
        return getError(serviceErrors.NOTHING_TO_UPDATE);
      }

      await banData.update(userId, filtered);

      if (filtered.banned) {
        await pointsService.deductBan(userId);
      }
    } else {
      await banData.create(userId, details.description, details.expiration);
      await pointsService.deductBan(userId);
    }

    // Need to get the user again because the points are updated
    const updatedUser = await userData.getBy('id', userId);
    const updatedBan = await banData.get(userId);

    return getSuccess({ ...updatedUser, banstatus: updatedBan });
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};
