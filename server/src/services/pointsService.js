import * as pointsData from '../data/pointsData.js';
import * as banData from '../data/banData.js';

const config = {
  return: 5,
  review: 10,
  like: 2,
  unlike: -2,
  banPercent: 0.01,
};

const update = async (userId, newPoints) => {
  const user = await pointsData.get(userId);
  const updatedPoints = user.points + newPoints;

  return pointsData.update(userId, updatedPoints);
};

export const awardReturning = async (userId) => {
  return update(userId, config.return);
};

export const awardReviewing = async (userId) => {
  return update(userId, config.review);
};

export const awardLike = async (userId) => {
  return update(userId, config.like);
};

export const deductUnlike = async (userId) => {
  return update(userId, config.unlike);
};

export const deductBan = async (userId) => {
  const user = await pointsData.get(userId);
  const currentPoints = user.points;
  const banstatus = await banData.get(userId);

  if (!banstatus.banned) {
    return false;
  }

  const msPerDay = 1000 * 60 * 60 * 24;
  const remainingDays = Math.floor((banstatus.expiration - Date.now()) / msPerDay);
  const accumulatedPercent = config.banPercent * remainingDays;
  const updatedPoints = Math.floor(currentPoints - currentPoints * accumulatedPercent);

  return await pointsData.update(userId, updatedPoints);
};
