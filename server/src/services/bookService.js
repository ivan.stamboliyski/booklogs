import { bookStatus, dbErrors, serviceErrors } from '../constants/constants.js';
import * as bookData from '../data/bookData.js';
import * as reviewData from '../data/reviewData.js';
import * as ratingData from '../data/ratingData.js';
import { getAllWithVotes, getError, getSuccess, getUpdatedProps } from './common.js';
import * as points from './pointsService.js';

export const addBook = async (title, author, status = bookStatus.available, cover = null) => {
  try {
    const bookId = await bookData.create(title, author, status, cover);

    if (bookId) {
      return getSuccess({ id: bookId, title, author, status, cover });
    }
  } catch (error) {
    if (error === dbErrors.SQLITE_CONSTRAINT) {
      return getError(serviceErrors.INVALID_DATA);
    }
    return getError(serviceErrors.SQL_ERROR);
  }

  return getError(serviceErrors.UNKNOWN_ERROR);
};

export const search = async (query, withDeleted = false) => {
  try {
    // canttouchthis:D destructuring with naming and assigning default values
    const { offset = 0, count = 20, order_by: orderBy = 'id', order_dir: orderDir = 'ASC', ...queries } = query;

    const books = await bookData.search(queries, offset, count, orderBy, orderDir, withDeleted);
    const countAll = await bookData.getCount();
    const payload = {
      books,
      pagination: {
        totalCount: countAll.total,
        count: books.length,
        offset: offset,
      },
    };

    return getSuccess(payload);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const getById = async (bookId) => {
  try {
    const book = await bookData.getById(bookId);

    if (!book) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    const reviews = await reviewData.getByBookId(bookId);
    const reviewsWithVotes = await getAllWithVotes(reviews);
    const rating = await ratingData.getAvg(bookId);

    return getSuccess({
      ...book,
      rating: rating.value,
      reviews: reviewsWithVotes,
    });
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const update = async (bookId, updates) => {
  try {
    const bookStored = await bookData.getById(bookId, true);

    if (!bookStored) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    const filtered = getUpdatedProps(bookStored, updates);

    if (Object.keys(filtered).length > 0) {
      if (await bookData.update(bookId, filtered)) {
        return getSuccess({ ...bookStored, ...filtered });
      }

      return getError(serviceErrors.NO_DATA_UPDATED);
    }

    return getError(serviceErrors.NOTHING_TO_UPDATE);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const remove = async (id) => {
  try {
    const book = await bookData.getById(id);

    if (!book) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    await bookData.remove(id);

    return getSuccess({ ...book, deleted: 1 });
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const borrow = async (bookId, userId) => {

  try {
    const book = await bookData.getById(bookId);

    if (!book || book.status === bookStatus.unlisted) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    if (book.status === bookStatus.available) {
      await bookData.updateBookStatus(bookId, bookStatus.borrowed, userId);
      // adds entry to the rating table
      await addOrUpdateRating(userId, bookId);

      return getSuccess({
        ...book,
        status: bookStatus.borrowed,
        userId,
      });
    }
    return getError(serviceErrors.OPERATION_NOT_PERMITTED);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const returnBook = async (bookId, userId) => {
  try {
    const book = await bookData.getById(bookId);

    if (!book) {
      return getError(serviceErrors.RECORD_NOT_FOUND);
    }

    if (book.userId !== userId) {
      return getError(serviceErrors.OPERATION_NOT_PERMITTED);
    }

    if (book.status === bookStatus.borrowed) {
      await bookData.updateBookStatus(bookId, bookStatus.available, null);
      // stores the return date in the rating table to enable users to rate books
      await addOrUpdateRating(userId, bookId, Date.now());
      await points.awardReturning(userId);

      return getSuccess({
        ...book,
        status: bookStatus.available,
        userId: null,
      });
    }

    return getError(serviceErrors.OPERATION_NOT_PERMITTED);

  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

export const rate = async (userId, bookId, value) => {
  try {
    const ratingStored = await ratingData.get(userId, bookId);

    if (!ratingStored) {
      // User has not yet borrowed the book
      return getError(serviceErrors.RATING_NOT_PERMITTED);
    }

    if (!ratingStored.returnDate) {
      // User has not yet returned the book
      return getError(serviceErrors.RATING_NOT_PERMITTED);
    }

    const book = await bookData.getById(bookId);
    const review = await reviewData.getByUserId(userId);

    if (!book || !review) {
      // User has not yet reviewed the book or book/user is deleted
      getError(serviceErrors.RATING_NOT_PERMITTED);
    }

    if (await ratingData.updateRating(userId, bookId, value)) {

      const ratingUpdated = await ratingData.getAvg(bookId);
      book.rating = ratingUpdated.value;

      return getSuccess(book);
    };

    return getError(serviceErrors.NO_DATA_UPDATED);
  } catch (error) {
    return getError(serviceErrors.SQL_ERROR);
  }
};

const addOrUpdateRating = async (userId, bookId, date = null) => {
  const rating = await ratingData.get(userId, bookId);

  if (rating) {
    await ratingData.updateReturn(userId, bookId, date);
  } else {
    await ratingData.add(userId, bookId);
  }
};
