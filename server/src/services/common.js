import * as reviewLikesData from '../data/reviewLikesData.js';

/**
 * Generate error object
 * @param {string} error Error message
 * @param {Object} data
 * @return {Object}
 */
export const getError = (error, data = null) => {
  return {
    error,
    data,
  };
};

export const getSuccess = (data) => {
  return {
    error: null,
    data,
  };
};

// Remove properties from updated, which match the value in stored
// Also removes properties from updated, which are not present on stored object
export const getUpdatedProps = (stored, updated) => {
  return Object.entries(updated)
    .reduce((acc, [key, value]) => {
      if (stored[key] !== undefined && value !== undefined && stored[key] !== value) {
        acc[key] = value;
      }
      return acc;
    }, {});
};

export const getWithVotes = async (review) => {
  const votes = await reviewLikesData.getAll(review.userId, review.bookId);

  return { ...review, votes };
};

export const getAllWithVotes = async (reviews) => {
  return await Promise.all(reviews.map(review => getWithVotes(review)));
};
