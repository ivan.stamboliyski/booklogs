import { getSql, insertSql, runSql } from './db-processing.js';

export const create = (userId, description, expiration) => {
  const query = `INSERT INTO banstatus(banned,user_id,description,expiration)
    VALUES(1,?,?,?)`;
  const values = [userId, description, expiration];

  return insertSql(query, values);
};

export const get = async (userId) => {
  const query = `SELECT user_id as userId, banned, description, expiration
    FROM banstatus
    WHERE user_id = ${userId}`;

  return (await getSql(query) || null);
};

export const update = async (userId, updates) => {
  const set = Object.keys(updates)
    .map((key) => `${key} = ?`)
    .join(', ');

  const query = `UPDATE banstatus
    SET ${set}
    WHERE user_id = ${userId}`;

  const values = Object.values(updates);

  return !!await runSql(query, values);
};

export const remove = async (userId) => {
  const query = `UPDATE banstatus 
    SET banned = 0 
    WHERE user_id = ${userId}`;

  return !!await runSql(query);
};
