import { allSql, getSql, runSql } from './db-processing.js';

export const add = (userId, reviewerId, bookId) => {
  const query = `INSERT INTO review_likes(user_id,reviewer_id,book_id)
    VALUES(?,?,?)`;
  const values = [userId, reviewerId, bookId];

  return runSql(query, values);
};

export const get = (userId, reviewerId, bookId) => {
  const query = `SELECT * FROM review_likes
    WHERE user_id = ? AND reviewer_id = ? AND book_id = ?`;
  const values = [userId, reviewerId, bookId];

  return getSql(query, values);
};

/**
 * Returns an array of likes for a specific review of a book.
 * Used to populate the votes property of reviews.
 * @param {number} reviewerId
 * @param {number} bookId
 * @return {Promise<Array>}
 */
export const getAll = (reviewerId, bookId) => {
  const query = `SELECT u.id as userId, u.username, u.avatar
    FROM review_likes l
    JOIN users u ON u.id = l.user_id
    WHERE l.reviewer_id = ? AND l.book_id = ? AND l.deleted != 1`;
  const values = [reviewerId, bookId];

  return allSql(query, values);
};

export const update = (userId, reviewerId, bookId, toggle) => {
  const query = `UPDATE review_likes
    SET deleted = ?
    WHERE user_id = ? AND reviewer_id = ? AND book_id = ?`;
  const values = [toggle, userId, reviewerId, bookId];

  return runSql(query, values);
};
