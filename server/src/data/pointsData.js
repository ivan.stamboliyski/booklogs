import { getSql, runSql } from './db-processing.js';

/**
 * @param {number} userId
 * @param {number} points
 * @return {Promise<boolean>}
 */
export const update = async (userId, points) => {
  const query = `UPDATE users
    SET reading_points = ${points}
    WHERE id = ${userId}`;

  return !!await runSql(query);
};

export const get = async (userId) => {
  const query = `SELECT id, username, reading_points as points 
    FROM users 
    WHERE id = ${userId};`;

  return getSql(query);
};
