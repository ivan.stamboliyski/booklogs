import { getSql, insertSql, runSql } from './db-processing.js';

export const add = (userId, bookId) => {
  const query = `INSERT INTO ratings (user_id,book_id)
    VALUES (${userId},${bookId})`;

  return insertSql(query);
};

export const get = (userId, bookId) => {
  const query = `SELECT user_id as userId, book_id as bookId, value, return_date as returnDate
    FROM ratings
    WHERE user_id = ${userId} AND book_id = ${bookId} AND deleted = 0`;

  return getSql(query);
};

export const getAvg = (bookId) => {
  // Precision hard-coded to 2
  const query = `SELECT ROUND(AVG(value),2) as value
  FROM ratings 
  WHERE book_id = ${bookId}`;

  return getSql(query);
};

export const updateReturn = async (userId, bookId, date) => {
  const query = `UPDATE ratings
    SET return_date = ${date}
    WHERE user_id = ${userId} AND book_id = ${bookId}`;

  return !!await runSql(query);
};

export const updateRating = async (userId, bookId, rating) => {
  const query = `UPDATE ratings
    SET value = ${rating}
    WHERE user_id = ${userId} AND book_id = ${bookId}`;

  return !!await runSql(query);
};

export const remove = async (userId, bookId) => {
  const query = `UPDATE ratings
    SET deleted = 1
    WHERE user_id = ${userId} AND book_id = ${bookId}`;

  return !!await runSql(query);
};
