import { getSql, insertSql, runSql } from './db-processing.js';

export const add = (token) => {
  const query = `INSERT INTO tokens(token) VALUES(?)`;
  const values = [token];

  return insertSql(query, values);
};

export const remove = async (token) => {
  const query = `DELETE FROM tokens WHERE token = ?`;
  const values = [token];

  return !!await runSql(query, values);
};

export const getToken = (token) => {
  const query = `SELECT * FROM tokens WHERE token = ?`;
  const values = [token];

  return getSql(query, values);
};
