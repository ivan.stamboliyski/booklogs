import { roles } from '../constants/constants.js';
import { allSql, getSql, insertSql, runSql } from './db-processing.js';

/**
 * @param {string} username
 * @param {string} password
 * @param {string} avatar
 * @param {roles} role
 * @return {Promise}
 */
export const create = (username, password, avatar, role = roles.user) => {
  const query = `INSERT INTO users(username,password,avatar,role)
    VALUES(?,?,?,?)`;
  const values = [username, password, avatar, role];

  return insertSql(query, values);
};
export const get = () => {
  const query = `SELECT id, username, role, avatar, reading_points as points
    FROM users
    WHERE deleted = 0`;

  return allSql(query);
};

/**
 * @param {string} column
 * @param {string} value
 * @return {Promise}
 */
export const getAll = () => {
  const query = `SELECT id, username, role, avatar, reading_points as points, deleted
    FROM users`;

  return allSql(query);
};

/**
 * Note that it returns deleted users too, might need to check user.deleted
 * @param {string} column
 * @param {any} value
 * @return {Promise<any>}
 */
export const getByWithPassword = (column, value) => {
  const query = `SELECT id, username, password, role, avatar, reading_points as points, deleted
    FROM users
    WHERE ${column} = $value`;
  const values = { $value: value };

  return getSql(query, values);
};

export const getBy = (column, value) => {
  const query = `SELECT id, username, role, avatar, reading_points as points
    FROM users
    WHERE ${column} = ? AND deleted = 0
  `;
  const values = [value];

  return getSql(query, values);
};

export const update = async (userId, updates) => {
  const set = Object.keys(updates)
    .map((key) => `${key} = ?`)
    .join(', ');

  const query = `UPDATE users
  SET ${set}
  WHERE id = ${userId}`;
  const values = Object.values(updates);

  return !!await runSql(query, values);
};

export const remove = async (userId) => {
  const query = `UPDATE users
    SET deleted = 1
    WHERE id = ${userId}`;

  return !!await runSql(query);
};
