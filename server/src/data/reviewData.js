import { allSql, getSql, insertSql, runSql } from './db-processing.js';

const selectQuery = `SELECT r.title, r.content, r.user_id as userId, u.username, u.avatar, r.book_id as bookId, b.title as bookTitle, b.cover
FROM reviews r
JOIN books b ON b.id = r.book_id
JOIN users u ON u.id = r.user_id`;

export const create = (userId, bookId, draft) => {
  const query = `INSERT INTO reviews(title,content,user_id,book_id)
    VALUES($title,$content,${userId},${bookId})`;
  const values = {
    $title: draft.title,
    $content: draft.content,
  };

  return insertSql(query, values);
};

export const getByBookId = (id) => {
  const query = `${selectQuery} WHERE r.book_id = ? AND r.deleted = 0`;
  const values = [id];

  return allSql(query, values);
};

export const getByUserId = (id) => {
  const query = `${selectQuery} WHERE r.user_id = ? AND r.deleted = 0`;
  const values = [id];

  return allSql(query, values);
};

export const getSingle = (userId, bookId) => {
  const query = `${selectQuery} WHERE r.user_id = ? AND r.book_id = ? AND r.deleted = 0`;
  const values = [userId, bookId];

  return getSql(query, values);
};

export const getWithDeleted = (userId, bookId) => {
  const query = `SELECT r.title, r.content, r.user_id as userId, u.username, u.avatar, r.book_id as bookId, b.title as bookTitle, b.cover, r.deleted
    FROM reviews r
    JOIN books b ON b.id = r.book_id
    JOIN users u ON u.id = r.user_id
    WHERE r.user_id = ? AND r.book_id = ?`;
  const values = [userId, bookId];

  return getSql(query, values);
};

export const getAll = () => {
  const query = `${selectQuery} WHERE r.deleted = 0`;

  return allSql(query);
};

export const update = (userId, bookId, updates) => {
  const set = Object.keys(updates)
    .map((key) => `${key} = ?`)
    .join(', ');
  const values = Object.values(updates);

  // Need to set deleted to 0,
  // for when a user had a review they deleted
  // and are now adding a new one
  const query = `UPDATE reviews 
    SET ${set}, deleted = 0
    WHERE user_id = ${userId} AND book_id = ${bookId} `;

  return runSql(query, values);
};

export const remove = (userId, bookId) => {
  const query = `UPDATE reviews
    SET deleted = 1
    WHERE user_id = ? AND book_id = ?`;
  const values = [userId, bookId];

  return runSql(query, values);
};
