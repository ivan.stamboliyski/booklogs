
import { bookStatus } from '../constants/constants.js';
import { allSql, getSql, insertSql, runSql } from './db-processing.js';

export const create = (name, author, status, cover) => {
  const query = `INSERT INTO books(title,author,status,cover) 
  VALUES ($name,$author,$status,$cover)`;
  const values = { $name: name, $author: author, $status: status, $cover: cover };

  // using a template string is possible but not recommended
  // because values in template string are not sanitised (' is not escaped)
  // const query = `INSERT INTO books(name) VALUES (${name})`;
  return insertSql(query, values);
};

const processParams = (params) => {
  const filters = Object.keys(params)
    .map((key) => `${key} LIKE ?`)
    .join(' AND ');
  const values = Object.values(params).map(v => `%${v.toLowerCase()}%`);

  return [filters, values];
};

// Includes rating
export const search = (params = {}, offset, count, orderBy, orderDir, withDeleted) => {
  const [filters, values] = processParams(params);
  const deletedCol = withDeleted ? ', b.deleted' : '';
  const deletedClause = withDeleted ? '' : `WHERE b.deleted = 0 AND b.status != '${bookStatus.unlisted}'`;
  const word = deletedClause && filters ? 'AND' : !!deletedClause && filters ? 'WHERE' : '';
  const namedOrder = orderBy === 'rating' ? orderBy : `b.${orderBy}`;

  const query = `SELECT  b.id, b.title, b.author, b.status, b.cover, ROUND(AVG(r.value),2) as rating ${deletedCol}
    FROM books b
    LEFT JOIN ratings r ON r.book_id = b.id
    ${deletedClause} ${word} ${filters}
    GROUP BY b.id
    ORDER BY ${namedOrder} ${orderDir}
    LIMIT ${offset}, ${count};`;

  return allSql(query, values);
};

export const getById = (id, withDeleted = false) => {
  const query = `SELECT id, title, author, status, cover,
    user_id as userId ${withDeleted ? ', deleted' : ''}
    FROM books
    WHERE id = $id ${withDeleted ? '' : 'AND deleted = 0'}`;
  const values = { $id: id };

  return getSql(query, values);
};

export const getCount = () => {
  const query = `SELECT COUNT(*) as total FROM books;`;

  return getSql(query);
};

export const update = async (bookId, updates) => {
  const set = Object.keys(updates)
    .map((key) => `${key} = ?`)
    .join(', ');

  const query = `UPDATE books
    SET ${set}
    WHERE id = ${bookId}`;
  const values = Object.values(updates);

  return !!await runSql(query, values);
};

export const remove = (id) => {
  const query = `UPDATE books
    SET deleted = 1
    WHERE id = ${id}`;

  return runSql(query);
};

export const updateBookStatus = (id, status, userId) => {
  const query = `UPDATE books 
    SET status = '${status}', user_id = ${userId}
    WHERE id = ${id} AND status != '${status}'`;

  return runSql(query);
};
