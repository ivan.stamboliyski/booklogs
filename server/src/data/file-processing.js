import fs from 'fs/promises';
import { extname } from 'path';
import multer from 'multer';

// create file or delete contents
export const createFile = async (fileName) => {
  await fs
    .writeFile(fileName, '')
    .then(() => console.log(`Empty file created ${fileName}`))
    .catch(err => console.log(err));
};

export const readFile = async (fileName) => {
  const contents = await fs
    .readFile(fileName)
    .then(d => d.toString())
    .catch(err => console.log(err));

  console.log(`File read ${fileName}`);

  return contents;
};


// File storage for multer
// To prevent file upload error when no file selected - do not send requests with avatar/cover property which is empty
const uploadFolder = 'covers/';
const storage = multer.diskStorage({
  destination(req, file, done) {
    done(null, uploadFolder);
  },
  filename(req, file, done) {
    done(
      null,
      Date.now() + extname(file?.originalname),
    );
  },
});

export const fileUploadMiddleware = multer({ storage });
