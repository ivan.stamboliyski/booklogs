// https://github.com/mapbox/node-sqlite3/wiki/API
// https://levelup.gitconnected.com/running-sql-queries-from-an-sql-file-in-a-nodejs-app-sqlite-a927f0e8a545

import sqlite3 from 'sqlite3';
import { dbFileName } from '../config.js';

const path = './database/';

console.log(`Connecting to db: ${path}${dbFileName}`);
const db = new sqlite3.Database(`${path}${dbFileName}`);
db.get('PRAGMA foreign_keys = ON');


/**
 * Runs the SQL query with the specified parameters.
 * Does not retrieve result data. Resolves to last id.
 * @param {string} query
 * @param {object|array} values
 * @return {Promise<number>} Promise resolves with id of row created
 */
export const insertSql = (query, values = []) => {
  console.log(query);
  return new Promise((resolve, reject) => {
    // NB!: DO NOT use arrow function for the love of this! (•_•)
    db.run(query, values, function(err) {
      if (err) {
        logError(values, err.message);
        reject(err.code);
      } else {
        // As per documentation this is Statement { lastID: number, changes: number }
        /* eslint no-invalid-this: 0 */
        resolve(this.lastID);
      }
    });
  });
};

/**
 * Runs the SQL query with the specified parameters.
 * Does not retrieve any data. Resolves to number of updates.
 * Use for UPDATE, DELETE
 * @param {string} query
 * @param {object|array} values
 * @return {Promise<number>} Promise resolves with number of changes done
 */
export const runSql = (query, values = []) => {
  console.log(query);
  return new Promise((resolve, reject) => {
    // NB!: DO NOT use arrow function for the love of this! (•_•)
    db.run(query, values, function(err) {
      if (err) {
        logError(values, err.message);
        reject(err.code);
      } else {
        // As per documentation this is Statement { lastID: number, changes: number }
        /* eslint no-invalid-this: 0 */
        resolve(this.changes);
      }
    });
  });
};

/**
 * Runs the SQL query with the specified parameters and
 * calls the callback with all result rows afterwards.
 * User for SELECT
 * @param {string} query
 * @param {object|array} values
 * @return {Promise} Promise resolves to ALL matching rows
 */
export const allSql = (query, values = []) => {
  console.log(query);

  return new Promise((resolve, reject) => {
    db.all(query, values, (err, rows) => {
      if (err) {
        logError(values, err.message);
        reject(err.code);
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
};

/**
 * Runs the SQL query with the specified parameters and
 * calls the callback with the FIRST result row afterwards.
 * Use for SELECT
 * @param {string} query
 * @param {object|array} values
 * @return {Promise} Promise resolves to the FIRST matching row
 */
export const getSql = (query, values = []) => {
  console.log(query);

  return new Promise((resolve, reject) => {
    db.get(query, values, (err, row) => {
      if (err) {
        logError(values, err.message);
        reject(err.code);
      } else {
        resolve(row);
      }
    });
  });
};

const logError = (values, message) => {
  console.log(`Error executing query values ${Object.values(values)}`);
  console.log(message);
};
