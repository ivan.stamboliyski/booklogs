// https://github.com/sqlitebrowser/sqlitebrowser/wiki
// https://www.sqlitetutorial.net/sqlite-nodejs/connect/

import sqlite3 from 'sqlite3';
import { dbFileName } from '../config.js';
import { createFile, readFile } from './file-processing.js';

const path = './database/';
const schemaSqlFile = 'init-schema.sql';
const dataSqlFile = 'init-data.sql';

const execSql = async (queries) => {
  console.log('Starting sql execution');

  return new Promise((resolve, reject) => {
    const startupDB = new sqlite3.Database(`${path}${dbFileName}`, sqlite3.OPEN_READWRITE, (err) => {
      if (err) {
        reject(err);
      }

      console.log(`Connected to ${dbFileName}`);
    });
    startupDB.get('PRAGMA foreign_keys = ON');
    startupDB.exec(queries, (err) => {
      if (err) {
        reject(err);
      }

      console.log('Query execution completed');
    }).close((err) => {
      if (err) {
        reject(err);
      }

      console.log(`DB connection closed ${dbFileName}\n`);
      resolve();
    });
  });
};

(async () => {
  try {
    console.log('Creating db schema');
    await createFile(path + dbFileName);
    const schemaQueries = await readFile(path + schemaSqlFile);
    await execSql(schemaQueries);

    console.log('Filling in initial data');
    const dataQueries = await readFile(path + dataSqlFile);
    await execSql(dataQueries);
  } catch (err) {
    console.log(err.message);
  }
})();
