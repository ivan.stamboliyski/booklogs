import e from 'express';
import { errorCodeMapping } from '../constants/constants.js';
import * as bookService from '../services/bookService.js';

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const create = async (req, res) => {
  const { title, author, status } = req.body;
  const cover = req.file?.filename;
  const { error, data: book } = await bookService.addBook(title, author, status, cover);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(book);
};

/**
 * Returns all (non-deleted) books or filters by
 * query parameters author, title etc.
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const search = async (req, res) => {
  const query = req.query;
  const { error, data: books } = await bookService.search(query);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(books);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const getById = async (req, res) => {
  const bookId = req.params.id;

  const { error, data: book } = await bookService.getById(bookId);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(book);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const searchAll = async (req, res) => {
  const query = req.query;
  const { error, data: books } = await bookService.search(query, true);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(books);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const update = async (req, res) => {
  const bookId = +req.params.id;
  const updates = req.body;
  updates.cover = req.file?.filename;

  const { error, data: updated } = await bookService.update(bookId, updates);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(updated);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const remove = async (req, res) => {
  const id = req.params.id;

  const { error, data: book } = await bookService.remove(id);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(book);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const borrow = async (req, res) => {
  const bookId = req.params.id;
  const userId = req.user?.id;

  const { error, data: book } = await bookService.borrow(bookId, userId);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(book);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const returnBook = async (req, res) => {
  const bookId = req.params.id;
  const userId = req.user?.id;

  const { error, data: book } = await bookService.returnBook(bookId, userId);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(book);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const rate = async (req, res) => {
  const userId = req.user?.id;
  const bookId = req.params.id;
  const value = req.body.value;

  const { error, data: book } = await bookService.rate(userId, bookId, value);

  error ?
    res.status(errorCodeMapping[error] || 500).send({ message: error }) :
    res.send(book);
};
