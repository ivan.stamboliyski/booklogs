import e from 'express';
import { errorCodeMapping } from '../constants/constants.js';
import * as reviewService from '../services/reviewService.js';


/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const create = async (req, res) => {
  const userId = req.params.userId || req.user?.id;
  const bookId = +req.params.id;
  const draft = {
    title: req.body.title,
    content: req.body.content,
  };

  const { error, data: review } = await reviewService.create(userId, bookId, draft);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(review);
  }
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const getAll = async (req, res) => {
  const { error, data: reviews } = await reviewService.getAll();

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(reviews);
  }
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const getByBookId = async (req, res) => {
  const id = req.params.id;
  const { error, data: reviews } = await reviewService.getByBookId(id);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(reviews);
  }
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const getByUserId = async (req, res) => {
  const id = req.params.id;
  const { error, data: reviews } = await reviewService.getByUserId(id);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(reviews);
  }
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const update = async (req, res) => {
  const userId = req.params.userId || req.user?.id;
  const bookId = req.params.id;
  const updates = req.body;

  const { error, data: updated } = await reviewService.update(userId, bookId, updates);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(updated);
  }
};


/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const remove = async (req, res) => {
  const userId = req.params.userId || req.user?.id;
  const bookId = req.params.id;

  const { error, data: review } = await reviewService.remove(userId, bookId);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(review);
  }
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const toggleLike = async (req, res) => {
  const userId = req.user?.id;
  const reviewerId = req.params.reviewerId;
  const bookId = req.params.id;

  const { error, data: review } = await reviewService.toggleLike(userId, reviewerId, bookId);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(review);
  }
};
