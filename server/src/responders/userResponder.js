import e from 'express';
import { errorCodeMapping } from '../constants/constants.js';
import * as userService from '../services/userService.js';

const respond = (res, error, data) => {
  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send(data);
  }
};

export const create = async (req, res) => {
  const { username, password } = req.body;
  const avatar = req.file?.filename || null;

  const { error, data: user } = await userService.create(username, password, avatar);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.status(201).send(user);
  }
};

export const logIn = async (req, res) => {
  const { username, password } = req.body;

  const { error, data: token } = await userService.logIn(username, password);

  respond(res, error, { token });
};

export const logOut = async (req, res) => {
  const token = req.headers.authorization?.split(' ')[1];
  const { error, data: message } = await userService.logOut(token);

  respond(res, error, message);
};

export const getUser = async (req, res) => {
  const userId = req.params.id;
  const { error, data: users } = await userService.getUser(userId);

  respond(res, error, users);
};

export const getUsers = async (_, res) => {
  const { error, data: users } = await userService.get();

  respond(res, error, users);
};

export const getUsersAll = async (_, res) => {
  const { error, data: users } = await userService.getAll();

  respond(res, error, users);
};

export const updateExtraProps = async (req, res) => {
  const userId = req.params.id;
  const updates = req.body;
  updates.avatar = req.file?.filename;
  const { error, data: updated } = await userService.update(userId, updates);

  respond(res, error, { message: 'Successfully updated user info, if role was changed, they need to log in again', updated });
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const updateUser = async (req, res) => {
  const loggedUser = req.user?.id;
  req.body.avatar = req.file?.filename;

  const updatesAllowed = ['username', 'password', 'avatar'];
  const updates = updatesAllowed.reduce((acc, key) => {
    if (req.body[key] !== undefined) {
      acc[key] = req.body[key];
    }
    return acc;
  }, {});

  const { error, data: user } = await userService.update(loggedUser, updates);

  if (error) {
    res.status(errorCodeMapping[error] || 500)
      .send({ message: error });
  } else {
    res.send({ message: 'Successfully updated user info, you need to log in again', user });
    const token = req.headers.authorization?.split(' ')[1];
    userService.logOut(token);
  }
};

export const remove = async (req, res) => {
  const userId = req.params.id;
  const { error, data: message } = await userService.remove(userId);

  respond(res, error, message);
};

/**
 * @param {e.Request} req
 * @param {e.Response} res
 */
export const banUser = async (req, res) => {
  const userId = +req.params.id;
  const details = req.body;

  const { error, data: user } = await userService.banUser(userId, details);

  respond(res, error, user);
};
