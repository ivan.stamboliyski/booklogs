export const errorMessages = {
  required: (name) => `${name} is required`,
  enumString: (name, values) => `${name} should be one of: ${Object.values(values).join(', ')}`,
};

export const validate = {
  stringRange: (name, value, min, max) => {
    if (typeof value === 'string' && value.length >= min && value.length <= max) {
      return null;
    }
    return `${name} should be a string in the range [${min}..${max}]`;
  },
  numberRange: (name, value, min, max) => {
    if (typeof value === 'number' && value >= min && value <= max) {
      return null;
    }

    return `${name} should be a number in the range [${min}..${max}]`;
  },
  booleanNumber: (name, value) => (+value === 0 || +value === 1) ? null : `${name} should be either 0 or 1`,
};
