import { errorMessages, validate } from './sharedValidations.js';

const minTitle = 5;
const maxTitle = 50;
const minContent = 10;
const maxContent = 500;

export const updateReviewSchema = {
  title: value => !value ? null : validate.stringRange('Title', value, minTitle, maxTitle),
  content: value => !value ? null : validate.stringRange('Content', value, minContent, maxContent),
};

export const createReviewSchema = {
  title: value => !value ? errorMessages.required('Title') : updateReviewSchema.title(value),
  content: value => !value ? errorMessages.required('Content') : updateReviewSchema.content(value),
};
