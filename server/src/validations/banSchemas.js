import { errorMessages, validate } from './sharedValidations.js';

const min = 5;
const max = 50;

export const updateBanSchema = {
  banned: value => value === undefined ? errorMessages.required('Banned') : validate.booleanNumber('Banned', value),
  description: value => !value ? errorMessages.required('Description') : validate.stringRange('Description', value, min, max),
  expiration: value => {
    if (!value) {
      return errorMessages.required('Expiration');
    }

    const threshold = Date.now();
    if (typeof value === 'number' && value > threshold) {
      return null;
    }

    return `Expiration should be a date in the future (milliseconds since January 1, 1970)`;
  },
};
