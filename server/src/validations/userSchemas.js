import { roles } from '../constants/constants.js';
import { errorMessages, validate } from './sharedValidations.js';

const min = 3;
const max = 25;

export const updateUserSchema = {
  username: value => !value ? null : validate.stringRange('Username', value, min, max),
  password: value => !value ? null : validate.stringRange('Password', value, min, max),
  role: value => !value || roles[value] ? null : errorMessages.enumString('Role', roles),
  deleted: value => !value ? null : validate.booleanNumber('Deleted', value),
  id: value => !value ? null : `Id cannot be updated`,
  banstatus: value => !value ? null : `Banstatus is updated from another endpoint`,
};

export const createUserSchema = {
  username: value => !value ? errorMessages.required('Username') : updateUserSchema.username(value),
  password: value => !value ? errorMessages.required('Password') : updateUserSchema.password(value),
};
