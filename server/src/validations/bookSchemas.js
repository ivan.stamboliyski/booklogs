import { bookStatus } from '../constants/constants.js';
import { errorMessages, validate } from './sharedValidations.js';

const min = 5;
const max = 50;

export const updateBookSchema = {
  title: value => !value ? null : validate.stringRange('Title', value, min, max),
  author: value => !value ? null : validate.stringRange('Author', value, min, max),
  status: value => !value || bookStatus[value] ? null : errorMessages.enumString('Status', bookStatus),
  deleted: value => !value ? null : validate.booleanNumber('Deleted', value),
  id: value => !value ? null : `Id cannot be updated`,
};

export const createBookSchema = {
  title: value => !value ? errorMessages.required('Title') : updateBookSchema.title(value),
  author: value => !value ? errorMessages.required('Author') : updateBookSchema.author(value),
};
