import { errorMessages, validate } from './sharedValidations.js';

const min = 1;
const max = 5;

export const ratingSchema = {
  value: value => !value ? errorMessages.required('Value') : validate.numberRange('Value', value, min, max),
};
