import e from 'express';

export const validator = (schema) => {

  /**
  * @type {inner}
  * @param {e.Request} req
  * @param {e.Response} res
  * @param {e.NextFunction} next
  * @return {void}
  */
  const inner = (req, res, next) => {
    const body = req.body;

    const errors = Object.keys(schema)
      .map(key => schema[key](body[key]))
      .filter(err => err !== null);

    if (errors.length > 0) {
      res.status(400).send(errors);
    } else {
      next();
    }
  };

  return inner;
};
