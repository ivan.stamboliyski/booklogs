import express from 'express';
import cors from 'cors';
import passport from 'passport';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';
import { PORT } from './config.js';
import { jwtStrategy } from './authentication/strategy.js';
import { adminRouter } from './routes/adminRouter.js';
import { bookRouter } from './routes/bookRouter.js';
import { userRouter } from './routes/userRouter.js';

const app = express();
const __dirname = dirname(fileURLToPath(import.meta.url));

passport.use(jwtStrategy);
app.use(cors(), express.json());
app.use(passport.initialize());
app.use('/covers', express.static(join(__dirname, '../', 'covers')));

const apiRouter = express.Router();

app.use('/api/v1', apiRouter);

apiRouter.use('/admin', adminRouter);
apiRouter.use('/books', bookRouter);
apiRouter.use('/users', userRouter);

app.all('*', (_, res) => {
  res.status(404).send({ message: 'Resource not found!' });
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
