import passport from 'passport';
import e from 'express';
import * as tokenData from '../data/tokenData.js';
import * as banData from '../data/banData.js';

// Documentation: http://www.passportjs.org/docs/authenticate/
/**
 * If authentication fails, Passport will respond with a 401 Unauthorized status,
 * any additional route handlers will not be invoked.
 * If authentication succeeds, the next handler will be invoked and
 * the req.user property will be set to the authenticated user.
 */
export const authMiddleware = passport.authenticate('jwt', { session: false });

/**
 * @param {string} roleName
 * @return {inner}
 */
export const roleMiddleware = roleName => {
  /**
   * @type {inner}
   * @param {e.Request} req
   * @param {e.Response} res
   * @param {e.NextFunction} next
   * @return {void}
   */
  const inner = (req, res, next) => {
    if (req.user && req.user.role === roleName) {
      next();
    } else {
      res.status(403).send({ message: 'Resource is forbidden for this type of user' });
    }
  };

  return inner;
};

/**
* @param {e.Request} req
* @param {e.Response} res
* @param {e.NextFunction} next
* @return {void}
*/
export const loggedInMiddleware = async (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1];
  const storedToken = await tokenData.getToken(token);
  if (token && storedToken) {
    next();
  } else {
    res.status(401).send({ message: 'Resource is private' });
  }
};

/**
* @param {e.Request} req
* @param {e.Response} res
* @param {e.NextFunction} next
* @return {void}
*/
export const banstatusMiddleware = async (req, res, next) => {
  const userId = req.user?.id;
  const banstatus = await banData.get(userId);

  // If !banned - ok, doesn't matter if time expired, someone manually removed the ban
  // If banned and time not expired - fail
  // If banned but time expired - ok
  if (banstatus && banstatus.banned && banstatus.expiration > Date.now()) {
    res.status(401).send({ message: 'This user is banned and can not edit' });
  } else {
    next();
  }
};
