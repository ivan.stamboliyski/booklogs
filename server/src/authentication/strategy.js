import passportJwt from 'passport-jwt';
import { PRIVATE_KEY } from '../config.js';

const options = {
  secretOrKey: PRIVATE_KEY,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

export const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const userData = {
    id: payload.sub,
    username: payload.username,
    role: payload.role,
  };

  // adds userData as req.user to the next middleware
  done(null, userData);
});
