import jwt from 'jsonwebtoken';
import { PRIVATE_KEY, TOKEN_LIFETIME } from '../config.js';

/**
 * @typedef Payload
 * @property {number} sub
 * @property {string} username
 * @property {roles} role
 */

/**
 * Generates JWT Token
 * @param {Payload} payload
 * @return {string}
 */
export const createToken = (payload) => {
  const options = { expiresIn: TOKEN_LIFETIME };
  const token = jwt.sign(payload, PRIVATE_KEY, options);

  return token;
};
