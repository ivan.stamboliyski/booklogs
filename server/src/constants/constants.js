export const bookStatus = {
  available: 'available',
  borrowed: 'borrowed',
  unlisted: 'unlisted',
};

export const roles = {
  admin: 'admin',
  user: 'user',
};

export const serviceErrors = {
  RECORD_NOT_FOUND: 'Record does not exist or is deleted',
  DUPLICATE_RECORD: 'The requirements allow only one of that resource',
  OPERATION_NOT_PERMITTED: 'Operation not permitted',
  RATING_NOT_PERMITTED: 'Rating is only possible if book was borrowed, returned, and reviewed',
  INVALID_SIGN_IN: 'username/password mismatch',
  INVALID_SIGN_OUT: 'User not signed in',
  INVALID_DATA: 'The provided data is invalid',
  NOTHING_TO_UPDATE: 'The provided update value is the same as the stored one',
  NO_DATA_UPDATED: 'Data was not updated',
  SQL_ERROR: 'Error retrieving/saving data',
  UNKNOWN_ERROR: 'Unknown error',
};

export const dbErrors = {
  SQLITE_CONSTRAINT: 'SQLITE_CONSTRAINT',
};

export const errorCodeMapping = {
  [serviceErrors.INVALID_SIGN_IN]: 400,
  [serviceErrors.INVALID_SIGN_OUT]: 400,
  [serviceErrors.NO_DATA_UPDATED]: 400,
  [serviceErrors.OPERATION_NOT_PERMITTED]: 403,
  [serviceErrors.RATING_NOT_PERMITTED]: 403,
  [serviceErrors.RECORD_NOT_FOUND]: 404,
  [serviceErrors.DUPLICATE_RECORD]: 409,
  [serviceErrors.NOTHING_TO_UPDATE]: 422,
  [serviceErrors.INVALID_DATA]: 422,
  [serviceErrors.SQL_ERROR]: 500,
  [serviceErrors.UNKNOWN_ERROR]: 500,
};
