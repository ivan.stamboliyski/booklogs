export const PORT = 5000;

// Documentation: https://www.npmjs.com/package/jsonwebtoken#jwtsignpayload-secretorprivatekey-options-callback
export const TOKEN_LIFETIME = '5 days';

export const PRIVATE_KEY = 'Wh3nUS1ng53nk3l3?2UL1st3ns';

export const dbFileName = 'librarita.db';
