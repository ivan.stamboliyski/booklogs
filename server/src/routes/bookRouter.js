import express from 'express';
import { authMiddleware, banstatusMiddleware, loggedInMiddleware } from '../authentication/middlewares.js';
import * as bookResponder from '../responders/bookResponder.js';
import * as reviewResponder from '../responders/reviewResponder.js';
import { validator } from '../validations/middleware.js';
import { ratingSchema } from '../validations/ratingSchema.js';
import { createReviewSchema, updateReviewSchema } from '../validations/reviewSchemas.js';

export const bookRouter = express.Router();

// token valid, user logged in
bookRouter.use(authMiddleware, loggedInMiddleware);

bookRouter
  .route('/')
  .get(bookResponder.search); // all books, searchable

bookRouter
  .route('/:id')
  .get(bookResponder.getById)
  .post(banstatusMiddleware, bookResponder.borrow)
  .delete(banstatusMiddleware, bookResponder.returnBook);

bookRouter
  .route('/:id/reviews')
  .get(reviewResponder.getByBookId);

// not for banned users from here on
bookRouter.use(banstatusMiddleware);

bookRouter
  // only currently logged-in user reviews
  .route('/:id/reviews')
  .post(validator(createReviewSchema), reviewResponder.create)
  .put(validator(updateReviewSchema), reviewResponder.update)
  .delete(reviewResponder.remove);

bookRouter
  // not possible to remove rating
  .put('/:id/rate', validator(ratingSchema), bookResponder.rate)
  .put('/:id/reviews/:reviewerId', reviewResponder.toggleLike);

