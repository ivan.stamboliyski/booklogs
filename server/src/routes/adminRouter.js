import express from 'express';
import { authMiddleware, banstatusMiddleware, loggedInMiddleware, roleMiddleware } from '../authentication/middlewares.js';
import { roles } from '../constants/constants.js';
import { createBookSchema, updateBookSchema } from '../validations/bookSchemas.js';
import { validator } from '../validations/middleware.js';
import * as bookResponder from '../responders/bookResponder.js';
import * as reviewResponder from '../responders/reviewResponder.js';
import * as userResponder from '../responders/userResponder.js';
import { updateUserSchema } from '../validations/userSchemas.js';
import { updateBanSchema } from '../validations/banSchemas.js';
import { fileUploadMiddleware } from '../data/file-processing.js';

export const adminRouter = express.Router();

// token valid, user logged in
adminRouter.use(authMiddleware, loggedInMiddleware);
// admin users only
adminRouter.use(roleMiddleware(roles.admin));

adminRouter
  .route('/books')
  .get(bookResponder.searchAll)
  .post(banstatusMiddleware, fileUploadMiddleware.single('cover'),
    validator(createBookSchema), bookResponder.create);

adminRouter
  .get('/reviews', reviewResponder.getAll)
  .get('/users', userResponder.getUsersAll);

// not for banned admins from here on
adminRouter.use(banstatusMiddleware);

adminRouter
  .route('/books/:id')
  .put(fileUploadMiddleware.single('cover'), validator(updateBookSchema), bookResponder.update)
  .delete(bookResponder.remove);

adminRouter
  .route('/books/:id/reviews/:userId')
  .post(reviewResponder.create)
  .put(reviewResponder.update)
  .delete(reviewResponder.remove);


adminRouter
  .route('/users/:id')
  .put(fileUploadMiddleware.single('avatar'), validator(updateUserSchema), userResponder.updateExtraProps)
  .delete(userResponder.remove);

adminRouter
  .route('/users/:id/ban')
  .put(validator(updateBanSchema), userResponder.banUser);
