import express from 'express';
import { authMiddleware, banstatusMiddleware, loggedInMiddleware } from '../authentication/middlewares.js';
import * as userResponder from '../responders/userResponder.js';
import * as reviewResponder from '../responders/reviewResponder.js';
import { createUserSchema, updateUserSchema } from '../validations/userSchemas.js';
import { validator } from '../validations/middleware.js';
import { fileUploadMiddleware } from '../data/file-processing.js';
export const userRouter = express.Router();

userRouter
  .post('/', fileUploadMiddleware.single('avatar'), validator(createUserSchema), userResponder.create)
  .post('/login', validator(createUserSchema), userResponder.logIn)
  .post('/logout', userResponder.logOut);

userRouter
  .use(authMiddleware, loggedInMiddleware)
  .get('/', userResponder.getUsers)
  .get('/:id', userResponder.getUser)
  .put('/', banstatusMiddleware, fileUploadMiddleware.single('avatar'),
    validator(updateUserSchema), userResponder.updateUser)
  .get('/:id/reviews', reviewResponder.getByUserId);
