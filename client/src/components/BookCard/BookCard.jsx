import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea, Link, Rating } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import { routes } from './../../common/constants.js';
import PropTypes from 'prop-types';

const BookCard = ({ book }) => {
  return (
    <Card
      sx={{
        maxWidth: { xs: 500, sm: 180 },
        height: 'auto',
        display: 'flex',
        flexDirection: 'column',
        bgcolor: '#f4d58d',
        boxShadow: 12,
        transition: 'transform .2s',
        '&:hover': { transform: 'scale(1.1)' },
      }}
    >
      <RouterLink to={`${routes.books}/${book.id}`}>
        <CardActionArea>
          <CardMedia
            height="200"
            component="img"
            image={`http://localhost:5000/covers/${book.cover}`}
            alt="random"
            sx={{
              objectFit: 'fill',
            }}
          />
        </CardActionArea>
      </RouterLink>
      <CardContent
        sx={{
          flexGrow: 1,
          bgcolor: '#f4d58d',
          pl: 2,
          textAlign: 'center',
        }}
      >
        <Typography
          gutterBottom
          variant="h5"
          component="h2"
          sx={{ textAlign: 'center', fontSize: '1.2em' }}
        >
          <Link
            href="#"
            underline="hover"
            variant="inerit"
            sx={{ color: 'black' }}
          >
            {book.title}
          </Link>
        </Typography>
        <Rating name="read-only" value={book.rating} readOnly />
        <br />
      </CardContent>
    </Card>
  );
};

BookCard.propTypes = {
  book: PropTypes.object.isRequired,
};

export default BookCard;
