import PropTypes from 'prop-types';
import { Popover, Rating, Typography } from "@mui/material";
import { useState } from "react";
import { canRate } from '../../common/helpers.js';
import { rateBook } from '../../services/requests.js';

const CustomRating = ({ book, setSnack, bannedUser }) => {
  const [rating, setRating] = useState(book.rating);
  const [anchorEl, setAnchorEl] = useState(null);

  const handlePopoverOpen = (event) => {
    if (canRate(book.id)) {
      setAnchorEl(event.currentTarget);
    }
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const handleRateBook = (value) => {
    const allowRating = canRate(book.id);
    if (allowRating) {
      return rateBook(book.id, value, setRating, setSnack);
    }

    setSnack({
      message: `A user can rate a book only if they have borrowed, returned and reviewed it.`,
      severity: 'error',
      open: true,
    });
  };

  return (
    <>
      <Rating
        name="simple-controlled"
        disabled={bannedUser}
        precision={0.5}
        value={rating}
        onChange={(event, newValue) => handleRateBook(newValue)}
        aria-owns={open ? 'mouse-over-popover' : undefined}
        aria-haspopup="true"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}
      />
      <Popover
        id="mouse-over-popover"
        sx={{
          pointerEvents: 'none',
        }}
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <Typography sx={{ p: 0.3 }}>Rate {book.title}</Typography>
      </Popover>
    </>
  );
};

CustomRating.propTypes = {
  book: PropTypes.object.isRequired,
  setSnack: PropTypes.func,
  bannedUser: PropTypes.bool,
};

export default CustomRating;
