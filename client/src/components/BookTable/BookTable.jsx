import { DataGrid } from '@mui/x-data-grid';
import Container from '@mui/material/Container';
import { useEffect, useState } from 'react';
import {
  adminCreateBook,
  adminDeleteBook,
  adminGetAllBooks,
  adminUpdateBook,
} from '../../services/requests';
import {
  Avatar,
  Chip,
  IconButton,
  Modal,
  Box,
  TextField,
  Typography,
  InputLabel,
  Rating,
  FormControl,
  NativeSelect,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { updateForm } from '../../common/helpers';
import AddIcon from '@mui/icons-material/Add';
import PropTypes from 'prop-types';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  maxWidth: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const BookTable = ({ setSnack }) => {
  const [books, setBooks] = useState([]);
  const [currentBook, setCurrentBook] = useState({
    id: null,
    title: '',
  });
  const [isAddNewBookActive, setIsAddNewBookActive] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [editModalForm, setEditModalForm] = useState({
    title: '',
    author: '',
    status: 'available',
    cover: '',
    deleted: '0',
  });

  useEffect(() => {
    adminGetAllBooks(setBooks, setSnack);
  }, [currentBook]);

  const handleOpenEditModal = (e) => {
    if (e.currentTarget.innerText === 'NEW BOOK') {
      setEditModalForm({
        title: '',
        author: '',
        status: 'available',
        cover: '',
        deleted: '0',
      });
      setIsAddNewBookActive(true);
    }

    setIsEditModalOpen(true);
  };

  const handleCloseEditModal = () => {
    if (isAddNewBookActive) {
      setIsAddNewBookActive(false);
    }

    setIsEditModalOpen(false);
  };

  const handleSaveEdit = (e) => {
    e.preventDefault();
    const form = new FormData(e.currentTarget);

    if (isAddNewBookActive) {
      adminCreateBook(form, setCurrentBook, setSnack);
    } else {
      adminUpdateBook(currentBook.id, form, setCurrentBook, setSnack);
    }

    setIsEditModalOpen(false);
  };

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 50,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'cover',
      headerName: 'Cover',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <Avatar
            variant="rounded"
            alt={cellValues.row.title}
            src={
              cellValues.value &&
              `http://localhost:5000/covers/${cellValues.value}`
            }
          />
        );
      },
    },
    ,
    {
      field: 'title',
      headerName: 'Title',
      width: 200,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'author',
      headerName: 'Author',
      width: 150,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'deleted',
      headerName: 'Deleted',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      hide: 'true',
    },
    {
      field: 'rating',
      headerName: 'Rating',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => (
        <Rating name="read-only" value={cellValues.value} readOnly />
      ),
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <Chip
            label={cellValues.row.deleted ? 'deleted' : cellValues.value}
            color={
              cellValues.row.deleted || cellValues.value !== 'available'
                ? 'error'
                : 'success'
            }
          />
        );
      },
    },
    {
      field: 'actions',
      headerName: 'Actions',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <>
            <IconButton
              onClick={(e) => {
                setCurrentBook(
                  books.find((book) => book.id === cellValues.row.id)
                );
                setEditModalForm({
                  author: cellValues.row.author,
                  cover: cellValues.row.cover,
                  title: cellValues.row.title,
                  status: cellValues.row.status,
                  deleted: cellValues.row.deleted,
                });
                handleOpenEditModal(e);
              }}
              color="primary"
              aria-label="edit"
            >
              <EditIcon />
            </IconButton>

            <IconButton
              onClick={() => {
                setCurrentBook(
                  books.find((book) => book.id === cellValues.row.id)
                );
                setOpenDeleteDialog(true);
              }}
              color="error"
              aria-label="delete"
            >
              <DeleteIcon />
            </IconButton>
          </>
        );
      },
    },
  ];

  return (
    <Container
      sx={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
      }}
    >
      <Box sx={{ alignSelf: 'flex-end', pr: 8, pb: 2 }}>
        <Button
          onClick={handleOpenEditModal}
          variant="outlined"
          color="success"
          startIcon={<AddIcon />}
          sx={{
            '&:hover': {
              bgcolor: '#3A6351',
              color: 'white',
            },
            color: '#3A6351',
            '&:disabled': { color: '#8d2222' },
            border: '1px solid black',
            mt: 1,
          }}
        >
          New Book
        </Button>
      </Box>
      <div
        style={{
          height: 600,
          width: '90%',
          backgroundColor: 'whitesmoke',
          alignSelf: 'center',
        }}
      >
        <DataGrid
          disableSelectionOnClick
          loading={books.length === 0}
          rows={books}
          columns={columns}
        />
      </div>

      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Delete book {currentBook.title} ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              adminDeleteBook(currentBook.id, setCurrentBook, setSnack);
              setOpenDeleteDialog(false);
            }}
          >
            Yes
          </Button>
          <Button onClick={() => setOpenDeleteDialog(false)} autoFocus>
            No
          </Button>
        </DialogActions>
      </Dialog>

      <Modal
        open={isEditModalOpen}
        id="edit-modal"
        onClose={handleCloseEditModal}
        aria-labelledby="edit-dialog-title"
        aria-describedby="edit-dialog-description"
      >
        <Box sx={style}>
          <Typography variant="h6">
            {isAddNewBookActive ? 'Add new book' : 'Edit book info'}
          </Typography>
          <hr />
          <Box component="form" onSubmit={handleSaveEdit}>
            <TextField
              error={
                editModalForm.title.length < 5 ||
                editModalForm.title.length > 50
              }
              helperText={
                (editModalForm.title.length < 5 ||
                  editModalForm.title.length > 50) &&
                'Title name must be between 5-50 characters'
              }
              id="title"
              name="title"
              label="Title"
              fullWidth
              value={editModalForm.title}
              onChange={(e) => {
                updateForm(setEditModalForm, 'title', e.target.value);
              }}
              sx={{ pb: 2 }}
            />

            <TextField
              error={
                editModalForm.author.length < 5 ||
                editModalForm.author.length > 50
              }
              helperText={
                (editModalForm.author.length < 5 ||
                  editModalForm.author.length > 50) &&
                'Author name must be between 5-50 characters'
              }
              id="author"
              name="author"
              label="Author"
              fullWidth
              value={editModalForm.author}
              onChange={(e) => {
                updateForm(setEditModalForm, 'author', e.target.value);
              }}
              sx={{ pb: 2 }}
            />

            <FormControl fullWidth sx={{ pb: 2 }}>
              <InputLabel variant="standard" htmlFor="uncontrolled-native">
                Status
              </InputLabel>
              <NativeSelect
                defaultValue={editModalForm.status}
                inputProps={{
                  name: 'status',
                  id: 'uncontrolled-native',
                }}
              >
                <option value="available">Available</option>
                <option value="borrowed">Borrowed</option>
                <option value="unlisted">Unlisted</option>
              </NativeSelect>
            </FormControl>

            <FormControl fullWidth sx={{ pb: 2 }}>
              <InputLabel variant="standard" htmlFor="select-deleted">
                Deleted
              </InputLabel>
              <NativeSelect
                defaultValue={editModalForm.deleted}
                inputProps={{
                  disabled: isAddNewBookActive,
                  name: 'deleted',
                  id: 'select-deleted',
                }}
              >
                <option value={1}>YES</option>
                <option value={0}>NO</option>
              </NativeSelect>
            </FormControl>

            <InputLabel
              shrink
              htmlFor="cover"
              sx={{ fontSize: '1.3em', fontWeight: 500 }}
            >
              Upload Cover
            </InputLabel>
            <TextField
              fullWidth
              name="cover"
              type="file"
              id="cover"
              autoComplete="new-cover"
              accept="image/*"
              sx={{ pb: 2 }}
            />
            <br />
            <Button type="submit" size="small" color="error">
              {isAddNewBookActive ? 'Add' : 'Save'}
            </Button>
            <Button onClick={handleCloseEditModal} size="small">
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </Container>
  );
};

BookTable.propTypes = {
  setSnack: PropTypes.func.isRequired,
};
export default BookTable;
