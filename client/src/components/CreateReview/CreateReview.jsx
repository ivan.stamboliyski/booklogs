import {
  Modal,
  Box,
  Button,
  TextField,
  Typography,
  Snackbar,
  Alert
} from "@mui/material";
import PropTypes from 'prop-types';
import { useContext } from 'react';
import { validateReviewInputs, updateForm } from "../../common/helpers.js";
import { postReview, updateReview } from './../../services/requests.js';
import AuthContext from './../../context/AuthContext.js';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  maxWidth: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const CreateReview = ({
  id,
  title,
  setBookReviews,
  isModalOpen,
  setModalOpen,
  snack,
  setSnack,
  hasToUpdate,
  setHasToUpdate,
  initialValues,
  reviewForm,
  setReviewForm,
}) => {
  const closeModal = () => {
    setModalOpen(false);
    setHasToUpdate(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (!validateReviewInputs(reviewForm.title, 'reviewTitle') ||
      !validateReviewInputs(reviewForm.content, 'reviewContent') ||
      Object.values(reviewForm).some(x => x.length === 0)) {
      return setSnack({ ...snack, message: 'You have to fill the fields correctly!', open: true });
    }

    let body = Object.entries({
      title: initialValues.title !== reviewForm.title ? reviewForm.title : '',
      content: initialValues.content !== reviewForm.content ? reviewForm.content : ''
    }).reduce((acc, value) => value[1].length > 0 ? { ...acc, [value[0]]: value[1] } : acc, {});

    if (hasToUpdate) {
      updateReview(id, body, setSnack, setBookReviews);
    } else {
      postReview(id, reviewForm, setSnack, setBookReviews, setReviewForm);
    }

    closeModal();
  };

  return (
    <>
      <Modal
        open={isModalOpen}
        onClose={() => closeModal()}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography variant='h6'>Write a review for {title}</Typography>
          <hr />
          <Box
            component="form"
          >
            <TextField
              error={!validateReviewInputs(reviewForm.title, 'reviewTitle')}
              helperText={
                !validateReviewInputs(reviewForm.title, 'reviewTitle') &&
                'Review title must be between 5-50 characters!'
              }
              onChange={(e) => updateForm(setReviewForm, 'title', e.target.value)}
              value={reviewForm.title}
              margin="normal"
              required
              fullWidth
              id="reviewTitle"
              label="Title"
              name="reviewTitle"
              autoComplete="reviewTitle"
              autoFocus
            />
            <TextField
              error={!validateReviewInputs(reviewForm.content, 'reviewContent')}
              helperText={
                !validateReviewInputs(reviewForm.content, 'reviewContent') &&
                'Review content must be between 10-500 characters!'
              }
              onChange={(e) => updateForm(setReviewForm, 'content', e.target.value)}
              value={reviewForm.content}
              minRows={5}
              required
              fullWidth
              multiline
              margin="normal"
              id="reviewTitle"
              label="Leave your thoughts on the book here"
              name="reviewTitle"
              autoComplete="reviewTitle"
            />
            <Button
              onClick={handleSubmit}
              size="small"
              sx={{
                ml: '8px',
                '&:hover': {
                  bgcolor: '#3A6351',
                  color: 'white',
                },
                color: '#3A6351',
                '&:disabled': { color: '#8d2222' },
                border: '1px solid black',
                mt: 1,
              }}
            >
              {hasToUpdate ? 'Update' : 'Submit'}
            </Button>
            <Button
              onClick={() => closeModal()}
              size="small"
              sx={{
                ml: '8px',
                '&:hover': {
                  bgcolor: '#3A6351',
                  color: 'white',
                },
                color: '#3A6351',
                '&:disabled': { color: '#8d2222' },
                border: '1px solid black',
                mt: 1,
              }}
            >
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={snack.open}
        autoHideDuration={4000}
        onClose={() => setSnack({ ...snack, message: '', open: false })}
      >
        <Alert
          onClose={() => setSnack({ ...snack, message: '', open: false })}
          severity={snack.severity}
          sx={{ width: '100%' }}
        >
          {snack.message}
        </Alert>
      </Snackbar>
    </>
  );
};

CreateReview.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  title: PropTypes.string,
  content: PropTypes.string,
  setBookReviews: PropTypes.func.isRequired,
  setSnack: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool,
  setModalOpen: PropTypes.func.isRequired,
  hasToUpdate: PropTypes.bool,
  setHasToUpdate: PropTypes.func.isRequired,
  snack: PropTypes.object.isRequired,
  initialValues: PropTypes.object.isRequired,
  reviewForm: PropTypes.object.isRequired,
  setReviewForm: PropTypes.func.isRequired,
};

export default CreateReview;
