import { DataGrid } from '@mui/x-data-grid';
import Container from '@mui/material/Container';
import { useEffect, useState } from 'react';
import {
  adminBanUser,
  adminDeleteUser,
  adminGetAllUsers,
  adminUnbanUser,
  adminUpdateUser,
} from '../../services/requests';
import {
  Avatar,
  Chip,
  IconButton,
  Modal,
  Box,
  TextField,
  Typography,
  InputLabel,
  FormControl,
  NativeSelect,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import BlockIcon from '@mui/icons-material/Block';
import CheckIcon from '@mui/icons-material/Check';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { updateForm, validateCredential } from '../../common/helpers';
import { minCredentialLength } from '../../common/constants';
import PropTypes from 'prop-types';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  maxWidth: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const UserTable = ({ setSnack }) => {
  const [users, setUsers] = useState([]);
  const [currentUser, setCurrentUser] = useState({
    username: '',
    id: null,
  });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [isBanDialogOpen, setIsBanDialogOpen] = useState(false);
  const [banModalForm, setBanModalForm] = useState({
    description: '',
    expiration: new Date().toISOString().slice(0, 10),
    banned: 1,
  });
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [editModalForm, setEditModalForm] = useState({
    username: '',
    role: '',
    avatar: '',
    deleted: '',
  });

  useEffect(() => {
    adminGetAllUsers(setUsers, setSnack);
  }, [currentUser]);

  const handleBanSubmit = (e) => {
    e.preventDefault();

    adminBanUser(
      currentUser.id,
      {
        ...banModalForm,
        expiration: new Date(banModalForm.expiration).getTime(),
      },
      setCurrentUser,
      setSnack
    );

    setIsBanDialogOpen(false);
  };

  const handleSaveEdit = (e) => {
    e.preventDefault();

    adminUpdateUser(
      currentUser.id,
      new FormData(e.currentTarget),
      setCurrentUser,
      setSnack
    );
    setIsEditModalOpen(false);
  };

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 50,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'avatar',
      headerName: 'Avatar',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <Avatar
            alt={cellValues.username}
            src={
              cellValues.value &&
              `http://localhost:5000/covers/${cellValues.value}`
            }
          />
        );
      },
    },
    ,
    {
      field: 'username',
      headerName: 'Username',
      width: 150,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'role',
      headerName: 'Role',
      width: 150,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'deleted',
      headerName: 'Deleted',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      hide: 'true',
    },
    {
      field: 'banstatus',
      headerName: 'Status',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <Chip
            label={
              cellValues.row.deleted
                ? 'deleted'
                : cellValues.value?.banned === 1
                  ? 'banned'
                  : 'active'
            }
            color={
              cellValues.row.deleted || cellValues.value?.banned === 1
                ? 'error'
                : 'success'
            }
          />
        );
      },
    },
    {
      field: 'actions',
      headerName: 'Actions',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <>
            <IconButton
              onClick={() => {
                setCurrentUser(users.find((u) => u.id === cellValues.row.id));
                setEditModalForm(cellValues.row);
                setIsEditModalOpen(true);
              }}
              color="primary"
              aria-label="edit"
            >
              <EditIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                setCurrentUser(users.find((u) => u.id === cellValues.row.id));

                cellValues.row.banstatus?.banned === 1
                  ? adminUnbanUser(cellValues.row.id, setCurrentUser, setSnack)
                  : setIsBanDialogOpen(true);
              }}
              aria-label="ban"
            >
              {cellValues.row.banstatus?.banned === 1 ? (
                <CheckIcon color="success" />
              ) : (
                <BlockIcon color="error" />
              )}
            </IconButton>
            <IconButton
              onClick={() => {
                setCurrentUser(users.find((u) => u.id === cellValues.row.id));
                setOpenDeleteDialog(true);
              }}
              color="error"
              aria-label="delete"
            >
              <DeleteIcon />
            </IconButton>
          </>
        );
      },
    },
  ];

  return (
    <Container sx={{ display: 'flex', justifyContent: 'center' }}>
      <div style={{ height: 600, width: '70%', backgroundColor: 'whitesmoke' }}>
        <DataGrid
          disableSelectionOnClick
          loading={users.length === 0}
          rows={users}
          columns={columns}
        />
      </div>

      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Delete user {currentUser.username} ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              adminDeleteUser(currentUser.id, setCurrentUser, setSnack);
              setOpenDeleteDialog(false);
            }}
          >
            Yes
          </Button>
          <Button onClick={() => setOpenDeleteDialog(false)} autoFocus>
            No
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={isBanDialogOpen}
        onClose={() => setIsBanDialogOpen(false)}
        aria-labelledby="dialog-title"
        aria-describedby="dialog-description"
      >
        <DialogContent>
          <Typography variant="h6">Ban {currentUser.username}</Typography>
          <hr />
          <Box
            component="form"
          >
            <TextField
              id="date"
              label="Expiration date"
              type="date"
              value={banModalForm.expiration}
              onChange={(e) => {
                updateForm(setBanModalForm, 'expiration', e.target.value);
              }}
              sx={{ width: 220 }}
              InputLabelProps={{
                shrink: true,
              }}
            />

            <TextField
              minRows={5}
              error={banModalForm.description.length < minCredentialLength}
              helperText={
                banModalForm.description.length < minCredentialLength &&
                'Description must be at least 3 characters'
              }
              required
              fullWidth
              multiline
              margin="normal"
              id="banDescription"
              label="Description"
              name="banDescription"
              autoComplete="banDescription"
              value={banModalForm.description}
              onChange={(e) => {
                updateForm(setBanModalForm, 'description', e.target.value);
              }}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleBanSubmit} size="small" color="error">
            Ban
          </Button>
          <Button onClick={() => setIsBanDialogOpen(false)} size="small">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>

      <Modal
        open={isEditModalOpen}
        id="edit-modal"
        onClose={() => setIsEditModalOpen(false)}
        aria-labelledby="edit-dialog-title"
        aria-describedby="edit-dialog-description"
      >
        <Box sx={style}>
          <Typography variant="h6">Edit user profile</Typography>
          <hr />
          <Box component="form" onSubmit={handleSaveEdit}>
            <TextField
              error={!validateCredential(editModalForm.username)}
              helperText={
                !validateCredential(editModalForm.username) &&
                'Username must be between 3-25 characters or more'
              }
              id="username"
              name="username"
              label="Username"
              fullWidth
              value={editModalForm.username}
              onChange={(e) => {
                updateForm(setEditModalForm, 'username', e.target.value);
              }}
              sx={{ pb: 2 }}
            />

            <FormControl fullWidth sx={{ pb: 2 }}>
              <InputLabel variant="standard" htmlFor="select-role">
                Role
              </InputLabel>
              <NativeSelect
                defaultValue={editModalForm.role}
                inputProps={{
                  name: 'role',
                  id: 'select-role',
                }}
              >
                <option value="user">User</option>
                <option value="admin">Admin</option>
              </NativeSelect>
            </FormControl>

            <FormControl fullWidth sx={{ pb: 2 }}>
              <InputLabel variant="standard" htmlFor="select-deleted-user">
                Deleted
              </InputLabel>
              <NativeSelect
                defaultValue={editModalForm.deleted}
                inputProps={{
                  name: 'deleted',
                  id: 'select-deleted-user',
                }}
              >
                <option value={1}>YES</option>
                <option value={0}>NO</option>
              </NativeSelect>
            </FormControl>

            <InputLabel
              shrink
              htmlFor="avatar"
              sx={{ fontSize: '1.3em', fontWeight: 500 }}
            >
              Upload avatar
            </InputLabel>
            <TextField
              fullWidth
              name="avatar"
              type="file"
              id="avatar"
              autoComplete="new-avatar"
              accept="image/*"
              sx={{ pb: 2 }}
            />
            <br />
            <Button type="submit" size="small" color="error">
              Save
            </Button>
            <Button onClick={() => setIsEditModalOpen(false)} size="small">
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </Container>
  );
};

UserTable.propTypes = {
  setSnack: PropTypes.func.isRequired,
};

export default UserTable;
