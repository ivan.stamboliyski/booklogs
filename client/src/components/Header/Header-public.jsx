import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import MenuBookRoundedIcon from '@mui/icons-material/MenuBookRounded';
import { Container } from '@mui/material';
import { Link } from 'react-router-dom';
import { routes } from '../../common/constants';
export default function PublicHeader() {
  return (
    <Box>
      <AppBar position="fixed" sx={{ bgcolor: '#242423' }}>
        <Container>
          <Toolbar>
            <Box sx={{ display: 'flex', flexGrow: 1 }}>
              <MenuBookRoundedIcon
                sx={{ display: { xs: 'none', sm: 'block' }, pr: 1 }}
              />
              <Typography
                variant="h6"
                component="div"
                sx={{ cursor: 'pointer' }}
              >
                <Link
                  to={routes.home}
                  style={{ color: 'white', textDecoration: 'none' }}
                >
                  booklogs
                </Link>
              </Typography>
            </Box>
            <Button color="inherit" sx={{ '&:hover': { bgcolor: '#3A6351' } }}>
              <Link
                to={routes.login}
                style={{ color: 'white', textDecoration: 'none' }}
              >
                Login
              </Link>
            </Button>
            <Button color="inherit" sx={{ '&:hover': { bgcolor: '#3A6351' } }}>
              <Link
                to={routes.signup}
                style={{ color: 'white', textDecoration: 'none' }}
              >
                Sign up
              </Link>
            </Button>
          </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
}
