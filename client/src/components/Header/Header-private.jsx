import * as React from 'react';
import PropTypes from 'prop-types';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import Button from '@mui/material/Button';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuBookRoundedIcon from '@mui/icons-material/MenuBookRounded';
import ListItemIcon from '@mui/material/ListItemIcon';
import Logout from '@mui/icons-material/Logout';
import {
  Avatar,
  Container,
  FormControl,
  InputLabel,
  Link,
  NativeSelect,
  Select,
} from '@mui/material';
import AuthContext from '../../context/AuthContext';
import { withRouter } from 'react-router';
import AppContext from '../../context/AppContext';
import { routes } from '../../common/constants';
import { getUserInfo } from '../../services/requests';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

function PrivateHeader({
  history,
  setKeyword,
  setFilterField,
  setUserDetails,
  setBannedUser,
}) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const { setAuth, user } = React.useContext(AuthContext);
  const { setPage, userDetails } = React.useContext(AppContext);
  const [field, setField] = React.useState('id');
  const [searchValue, setSearchValue] = React.useState('');
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  React.useEffect(() => {
    getUserInfo(user.id, setUserDetails);
  }, [setAuth]);

  const handleChange = (event) => {
    setField(event.target.value);
  };

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const updateSearch = (e) => {
    if (e.type === 'click') {
      setPage(1);
      setFilterField(field);
      setKeyword(searchValue);
      setSearchValue('');
      history.push(`${routes.books}/pages/1`);
    }

    if (e.key === 'Enter') {
      setPage(1);
      setFilterField(field);
      setKeyword(searchValue);
      setSearchValue('');
      history.push(`${routes.books}/pages/1`);
    }
  };

  const logout = () => {
    setAnchorEl(null);
    handleMobileMenuClose();

    localStorage.removeItem('library-token');
    setAuth({ user: null, isLoggedIn: false });
    setBannedUser(false);

    history.push('/');
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={logout}>
        <ListItemIcon>
          <Logout fontSize="small" />
        </ListItemIcon>
        Logout
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleProfileMenuOpen} sx={{ pr: 10 }}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <Avatar
            alt={user.username}
            src={
              userDetails.avatar &&
              `http://localhost:5000/covers/${userDetails.avatar}`
            }
          />
        </IconButton>
        Profile
      </MenuItem>
      <MenuItem
        onClick={() => {
          setKeyword('');
          history.push(`${routes.books}/pages/1`);
          setPage(1);
        }}
      >
        All Books
      </MenuItem>
      {user.role === 'admin' && (
        <MenuItem onClick={() => history.push('/admin')}>Admin</MenuItem>
      )}
    </Menu>
  );

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" color="primary" sx={{ bgcolor: '#242423' }}>
        <Container>
          <Toolbar>
            <Box
              onClick={() => {
                setKeyword('');
                history.push(`${routes.books}/pages/1`);
                setPage(1);
              }}
              sx={{ display: 'flex', cursor: 'pointer' }}
            >
              <MenuBookRoundedIcon
                sx={{ display: { xs: 'none', sm: 'block' }, pr: 1 }}
              />
              <Typography
                variant="h6"
                noWrap
                component="div"
                sx={{ display: { xs: 'none', sm: 'block' }, pr: 5 }}
              >
                booklogs
              </Typography>
            </Box>
            <Search>
              <StyledInputBase
                onKeyUp={updateSearch}
                value={searchValue}
                onChange={(e) => setSearchValue(e.target.value)}
                sx={{ width: ['70%', '80%', '85%'] }}
                placeholder="Search…"
                inputProps={{ 'aria-label': 'search' }}
              />
              <IconButton
                onClick={updateSearch}
                type="submit"
                sx={{
                  p: '6px',
                  color: 'white',
                  '&:hover': { bgcolor: 'black' },
                }}
                aria-label="search"
              >
                <SearchIcon />
              </IconButton>
            </Search>
            <FormControl variant="standard" sx={{ minWidth: 60 }}>
              <InputLabel sx={{ color: 'white' }} id="search-select">
                Search by
              </InputLabel>
              <Select
                labelId="search-select"
                id="select-standard"
                value={field}
                onChange={handleChange}
                label="Field"
                sx={{ color: 'white' }}
              >
                <MenuItem value="id">Id</MenuItem>
                <MenuItem value="author">Author</MenuItem>
                <MenuItem value="title">Title</MenuItem>
              </Select>
            </FormControl>
            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
              <Button
                onClick={() => {
                  setKeyword('');
                  history.push(`${routes.books}/pages/1`);
                  setPage(1);
                }}
                color="inherit"
                sx={{ '&:hover': { bgcolor: '#3A6351' } }}
              >
                All Books
              </Button>
              {user.role === 'admin' && (
                <Button
                  onClick={() => history.push('/admin')}
                  color="inherit"
                  sx={{ '&:hover': { bgcolor: '#3A6351' } }}
                >
                  Admin
                </Button>
              )}
              <IconButton
                size="large"
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
              >
                <Avatar
                  alt={user.username}
                  src={
                    userDetails.avatar &&
                    `http://localhost:5000/covers/${userDetails.avatar}`
                  }
                />
              </IconButton>
              <Typography
                onClick={handleProfileMenuOpen}
                variant="h4"
                sx={{ pt: '14px', fontSize: '1.5em' }}
              >
                {user.username}
              </Typography>
            </Box>
            <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                size="large"
                color="inherit"
                aria-label="open drawer"
              >
                <MenuIcon />
              </IconButton>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </Box>
  );
}

PrivateHeader.propTypes = {
  history: PropTypes.object.isRequired,
  setKeyword: PropTypes.func.isRequired,
  setFilterField: PropTypes.func.isRequired,
  setUserDetails: PropTypes.func.isRequired,
  setBannedUser: PropTypes.func.isRequired,
};

export default withRouter(PrivateHeader);
