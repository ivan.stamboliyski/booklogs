import { Redirect, Route } from 'react-router';
import PropTypes from 'prop-types';

const PrivateRoute = ({ component: Component, auth, props, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(routerProps) =>
        auth === true ? (<Component {...{ ...routerProps, ...props }} />) : (<Redirect to="/login" />)}
    />
  );
};

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  auth: PropTypes.bool.isRequired,
  props: PropTypes.object,
  rest: PropTypes.array,
};

export default PrivateRoute;
