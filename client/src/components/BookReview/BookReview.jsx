import {
  Avatar,
  Button,
  Container,
  Rating,
  Typography,
  Box,
  IconButton,
} from '@mui/material';
import PropTypes from 'prop-types';
import Badge from '@mui/material/Badge';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { useContext, useState } from 'react';
import AuthContext from '../../context/AuthContext.js';
import { likeReview } from '../../services/requests.js';

const BookReview = ({
  bookId,
  username,
  avatar,
  content,
  title,
  votes,
  userId,
  openModal,
  openDialog,
  setSnack,
  bannedUser,
}) => {
  const { user } = useContext(AuthContext);
  const [likeStatus, setLikeStatus] = useState(votes.find(u => u.userId === user.id));
  const [likesCount, setLikesCount] = useState(votes.length);

  const handleReviewVote = () => {
    setLikeStatus((prevValue) => !prevValue);
    likeReview(bookId, userId, setLikesCount, setSnack);
  };

  return (
    <Container>
      <Box
        sx={{
          display: 'flex',
          position: 'relative',
        }}
      >
        <Avatar sx={{ mb: '12px' }} alt={username} src={`http://localhost:5000/covers/${avatar}`} />
        <Typography sx={{ ml: '10px', fontSize: '22px' }}>{username}</Typography>
        <IconButton
          onClick={(event) => openModal(event)}
          color="primary"
          size="small"
          aria-label="edit"
          sx={{
            visibility:
              user.id === userId
                ? 'visible'
                : 'hidden',
            ml: 1,
          }}
        >
          <EditIcon />
        </IconButton>
        <IconButton
          onClick={(event) => openDialog(event)}
          color="error"
          size="small"
          aria-label="delete"
          sx={{
            visibility:
              user.id === userId
                ? 'visible'
                : 'hidden',
          }}
        >
          <DeleteIcon />
        </IconButton>
        <IconButton
          onClick={handleReviewVote}
          disabled={bannedUser}
          color="error"
          size="small"
          aria-label="delete"
          sx={{
            position: 'absolute',
            top: '0px',
            right: '0px',
          }}
        >
          <Badge
            badgeContent={likesCount}
            color="secondary"
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
          >
            {likeStatus ?
              <FavoriteIcon
                sx={{
                  color: '#ef5350',
                  fontSize: 33,
                  '&:hover': {
                    bgcolor: '#ef5350',
                    color: 'white',
                    borderRadius: '50%',
                  },
                }}
              /> :
              <FavoriteBorderIcon
                sx={{
                  color: '#ef5350',
                  fontSize: 33,
                  '&:hover': {
                    bgcolor: '#ef5350',
                    color: 'white',
                    borderRadius: '50%',
                  },
                }}
              />}
          </Badge>
        </IconButton>
      </Box>
      <Typography variant="h6" sx={{ mb: '12px' }}>{title}</Typography>
      <Typography sx={{ width: '100%', overflowWrap: 'break-word', }}>{content}</Typography>
      <hr />
    </Container>
  );
};

BookReview.propTypes = {
  bookId: PropTypes.number,
  username: PropTypes.string,
  avatar: PropTypes.string,
  title: PropTypes.string,
  content: PropTypes.string,
  votes: PropTypes.arrayOf(PropTypes.object),
  userId: PropTypes.number,
  openModal: PropTypes.func.isRequired,
  openDialog: PropTypes.func.isRequired,
  setSnack: PropTypes.func.isRequired,
  bannedUser: PropTypes.bool,
};

export default BookReview;
