import { DataGrid } from '@mui/x-data-grid';
import Container from '@mui/material/Container';
import { useEffect, useState } from 'react';
import {
  adminGetAllReviews,
  adminUpdateReview,
  adminDeleteReview,
} from '../../services/requests';
import {
  Avatar,
  Chip,
  IconButton,
  Modal,
  Box,
  TextField,
  Typography,
  InputLabel,
  Rating,
  FormControl,
  NativeSelect,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { updateForm } from '../../common/helpers';
import PropTypes from 'prop-types';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  maxWidth: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ReviewTable = ({ setSnack }) => {
  const [reviews, setReviews] = useState([]);
  const [currentReview, setCurrentReview] = useState({
    bookId: null,
    userId: null,
    title: '',
  });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [editModalForm, setEditModalForm] = useState({
    title: '',
    content: '',
  });

  useEffect(() => {
    adminGetAllReviews(setReviews, setSnack);
  }, [currentReview]);

  const handleOpenEditModal = () => {
    setIsEditModalOpen(true);
  };

  const handleCloseEditModal = () => {
    setIsEditModalOpen(false);
  };

  const handleSaveEdit = (e) => {
    e.preventDefault();

    adminUpdateReview(
      currentReview.bookId,
      currentReview.userId,
      editModalForm,
      setCurrentReview,
      setSnack
    );

    setIsEditModalOpen(false);
  };

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 50,
      align: 'center',
      headerAlign: 'center',
      hide: 'true',
    },
    {
      field: 'userId',
      headerName: 'USER_ID',
      width: 50,
      align: 'center',
      headerAlign: 'center',
      hide: 'true',
    },
    {
      field: 'bookId',
      headerName: 'BOOK_ID',
      width: 50,
      align: 'center',
      headerAlign: 'center',
      hide: 'true',
    },
    {
      field: 'bookTitle',
      headerName: 'Book Title',
      width: 200,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'avatar',
      headerName: 'Avatar',
      width: 80,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <Avatar
            alt={cellValues.username}
            src={
              cellValues.value &&
              `http://localhost:5000/covers/${cellValues.value}`
            }
          />
        );
      },
    },
    {
      field: 'username',
      headerName: 'Username',
      width: 140,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'title',
      headerName: 'Review Title',
      width: 150,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'content',
      headerName: 'Review Content',
      width: 300,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'actions',
      headerName: 'Actions',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <>
            <IconButton
              onClick={(e) => {
                setCurrentReview(
                  reviews.find(
                    (review) =>
                      review.bookId === cellValues.row.bookId &&
                      review.userId === cellValues.row.userId
                  )
                );
                setEditModalForm({
                  title: cellValues.row.title,
                  content: cellValues.row.content,
                });
                handleOpenEditModal(e);
              }}
              color="primary"
              aria-label="edit"
            >
              <EditIcon />
            </IconButton>

            <IconButton
              onClick={() => {
                setCurrentReview(
                  reviews.find(
                    (review) =>
                      review.bookId === cellValues.row.bookId &&
                      review.userId === cellValues.row.userId
                  )
                );
                setOpenDeleteDialog(true);
              }}
              color="error"
              aria-label="delete"
            >
              <DeleteIcon />
            </IconButton>
          </>
        );
      },
    },
  ];

  return (
    <Container
      sx={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
      }}
    >
      <div
        style={{
          height: 600,
          width: '90%',
          backgroundColor: 'whitesmoke',
          alignSelf: 'center',
        }}
      >
        <DataGrid
          disableSelectionOnClick
          loading={reviews.length === 0}
          rows={reviews.map((review) => ({
            ...review,
            id: review.bookId + review.userId,
          }))}
          columns={columns}
        />
      </div>

      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Delete review {currentReview.title} ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              adminDeleteReview(
                currentReview.bookId,
                currentReview.userId,
                setCurrentReview,
                setSnack
              );
              setOpenDeleteDialog(false);
            }}
          >
            Yes
          </Button>
          <Button onClick={() => setOpenDeleteDialog(false)} autoFocus>
            No
          </Button>
        </DialogActions>
      </Dialog>

      <Modal
        open={isEditModalOpen}
        id="edit-modal"
        onClose={handleCloseEditModal}
        aria-labelledby="edit-dialog-title"
        aria-describedby="edit-dialog-description"
      >
        <Box sx={style}>
          <Typography variant="h6">Edit review</Typography>
          <hr />
          <Box component="form" onSubmit={handleSaveEdit}>
            <TextField
              id="title"
              name="title"
              label="Title"
              fullWidth
              value={editModalForm.title}
              onChange={(e) => {
                updateForm(setEditModalForm, 'title', e.target.value);
              }}
              sx={{ pb: 2 }}
            />

            <TextField
              id="content"
              name="content"
              label="Content"
              fullWidth
              value={editModalForm.content}
              onChange={(e) => {
                updateForm(setEditModalForm, 'content', e.target.value);
              }}
              sx={{ pb: 2 }}
            />
            <br />
            <Button type="submit" size="small" color="error">
              Save
            </Button>
            <Button onClick={handleCloseEditModal} size="small">
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </Container>
  );
};

ReviewTable.propTypes = {
  setSnack: PropTypes.func.isRequired,
};

export default ReviewTable;
