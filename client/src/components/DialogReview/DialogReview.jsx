import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Slide
} from "@mui/material";
import PropTypes from 'prop-types';
import {
  postBorrow,
  returnBook,
  deleteReview
} from '../../services/requests.js';
import { useContext, forwardRef } from 'react';
import AuthContext from '../../context/AuthContext.js';
import { dialogMessages } from '../../common/constants.js';
const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const DialogReview = ({
  open,
  setOpen,
  book,
  setBook,
  setSnack,
  setBookReviews,
  deleteUserReview,
  setDeleteUserReview,
  dialogAction,
  setDialogAction,
  initialValues,
  setReviewForm }) => {
  const { user } = useContext(AuthContext);

  const closeDialog = () => {
    if (deleteUserReview) {
      setReviewForm({
        title: '',
        content: ''
      });
      setDeleteUserReview(false);

    } else {
      setReviewForm({
        title: initialValues.title || '',
        content: initialValues.content || ''
      });
    }

    setOpen(false);
    setDialogAction('');
  };

  const handleDialogRequest = () => {
    if (deleteUserReview) {
      deleteReview(book.id, setSnack, setBookReviews);
      setDeleteUserReview(false);

    } else {
      user.id !== book.userId
        ? postBorrow(book, setBook, setSnack)
        : returnBook(book, setBook, setSnack);
    }

    closeDialog();
  };

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={closeDialog}
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle>
        {dialogMessages.title[dialogAction]}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-slide-description">
          {dialogMessages.content[dialogAction]}{book.title}?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleDialogRequest}
        >
          YES
        </Button>
        <Button onClick={closeDialog}>NO</Button>
      </DialogActions>
    </Dialog>
  );
};

DialogReview.propTypes = {
  book: PropTypes.object.isRequired,
  open: PropTypes.bool,
  content: PropTypes.string,
  setBookReviews: PropTypes.func.isRequired,
  setSnack: PropTypes.func.isRequired,
  setOpen: PropTypes.func,
  initialValues: PropTypes.object,
  setReviewForm: PropTypes.func.isRequired,
  deleteUserReview: PropTypes.bool,
  setDeleteUserReview: PropTypes.func.isRequired,
  dialogAction: PropTypes.string,
  setDialogAction: PropTypes.func.isRequired,
  setBook: PropTypes.func.isRequired,
};

export default DialogReview;
