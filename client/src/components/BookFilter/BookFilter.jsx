import * as React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputBase from '@mui/material/InputBase';
import { Box } from '@mui/system';
import { IconButton } from '@mui/material';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import AppContext from '../../context/AppContext';

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  'label + &': {
    marginTop: theme.spacing(3),
  },
  '& .MuiInputBase-input': {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,0,0,.25)',
    },
  },
}));

function BookFilter({ setSortField, setSortDirection }) {
  const { sortField, sortDirection } = React.useContext(AppContext);

  const handleChange = (event) => {
    setSortField(event.target.value);
  };

  const handleDirectionChange = () => {
    if (sortDirection === 'ASC') {
      setSortDirection('DESC');
    } else {
      setSortDirection('ASC');
    }
  };

  return (
    <Box
      sx={{ display: 'flex', justifyContent: 'right', pr: [1, 1, 8], pb: 2 }}
    >
      <FormControl sx={{ m: 1, minWidth: 70 }} variant="standard">
        <InputLabel
          sx={{ color: 'black', '&:focus': { color: 'black' } }}
          id="select-label"
        >
          Sort by
        </InputLabel>
        <Select
          labelId="select-label"
          id="customized-select"
          value={sortField}
          onChange={handleChange}
          input={<BootstrapInput />}
        >
          <MenuItem value="id">Id</MenuItem>
          <MenuItem value="title">Title</MenuItem>
          <MenuItem value="author">Author</MenuItem>
          <MenuItem value="rating">Rating</MenuItem>
        </Select>
      </FormControl>
      <IconButton
        onClick={handleDirectionChange}
        aria-label="delete"
        size="large"
        sx={{ mt: 2 }}
      >
        {sortDirection === 'ASC' ? (
          <ArrowUpwardIcon fontSize="large" />
        ) : (
          <ArrowDownwardIcon fontSize="large" />
        )}
      </IconButton>
    </Box>
  );
}

BookFilter.propTypes = {
  setSortDirection: PropTypes.func.isRequired,
  setSortField: PropTypes.func.isRequired,
};

export default BookFilter;
