import BookTable from '../components/BookTable/BookTable';
import UserTable from '../components/UserTable/UserTable';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Box } from '@mui/system';
import { useState } from 'react';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import PeopleIcon from '@mui/icons-material/People';
import ReviewTable from '../components/ReviewTable/ReviewTable';
import { Snackbar, Alert } from '@mui/material';
const AdminView = () => {
  const [value, setValue] = useState('books');
  const [snack, setSnack] = useState({
    open: false,
    message: '',
    severity: 'error',
  });

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <h1 style={{ textAlign: 'center' }}>ADMIN PANEL</h1>
      <TabContext value={value}>
        <Box>
          <TabList onChange={handleChange} centered>
            <Tab icon={<MenuBookIcon />} label="BOOKS" value="books" />
            <Tab icon={<PeopleIcon />} label="USERS" value="users" />
            <Tab icon={<PeopleIcon />} label="REVIEWS" value="reviews" />
          </TabList>
        </Box>
        <TabPanel value="books">
          <BookTable setSnack={setSnack} />
        </TabPanel>
        <TabPanel value="users">
          <UserTable setSnack={setSnack} />
        </TabPanel>
        <TabPanel value="reviews">
          <ReviewTable setSnack={setSnack} />
        </TabPanel>
      </TabContext>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={snack.open}
        autoHideDuration={6000}
        onClose={() => setSnack({ ...snack, message: '', open: false })}
      >
        <Alert
          onClose={() => setSnack({ ...snack, message: '', open: false })}
          severity={snack.severity}
          sx={{ width: '100%' }}
        >
          {snack.message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default AdminView;
