export const Page404 = () => {
  return (
    <div
      style={{
        margin: 'auto',
        display: 'flex',
        justifyContent: 'center',
        maxWidth: 1200,
        maxHeight: 600,
        backgroundColor: 'lightgrey',
      }}
    >
      <img
        src="https://i.pinimg.com/originals/c9/27/89/c92789bb73d3442143f01da919c2f9e7.gif"
        alt="404"
      />
    </div>
  );
};
