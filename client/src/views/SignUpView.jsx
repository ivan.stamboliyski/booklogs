import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Link } from 'react-router-dom';
import { routes } from './../common/constants.js';
import { Alert, InputLabel, Snackbar } from '@mui/material';
import { validateCredential } from '../common/helpers.js';
import { updateForm } from '../common/helpers.js';

const theme = createTheme();
export default function SignUp() {
  const [userForm, setUserForm] = React.useState({
    username: '',
    password: '',
  });

  const [snacks, setSnacks] = React.useState({
    openError: false,
    error: '',
    openSuccess: false,
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch('http://localhost:5000/api/v1/users/', {
      method: 'POST',
      body: new FormData(event.currentTarget),
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.message) {
          throw new Error(data.message);
        }

        setSnacks({ ...snacks, openSuccess: true });
      })
      .catch(({ message }) => {
        setSnacks({ ...snacks, openError: true, error: message });
      });
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: '#242423' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  error={!validateCredential(userForm.username)}
                  helperText={
                    !validateCredential(userForm.username) &&
                    'Username must be between 3-25 characters or more'
                  }
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  name="username"
                  autoComplete="username"
                  onChange={(e) => updateForm(setUserForm, 'username', e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  error={!validateCredential(userForm.password)}
                  helperText={
                    !validateCredential(userForm.password) &&
                    'Password must be between 3-25 characters or more'
                  }
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                  onChange={(e) => updateForm(setUserForm, 'password', e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <InputLabel
                  shrink
                  htmlFor="avatar"
                  sx={{ fontSize: '1.3em', fontWeight: 500 }}
                >
                  Upload avatar
                </InputLabel>
                <TextField
                  required
                  fullWidth
                  name="avatar"
                  type="file"
                  id="avatar"
                  autoComplete="new-avatar"
                  accept="image/*"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link
                  to={routes.login}
                  variant="body2"
                  style={{ color: '#1976d2', fontSize: '0.875rem' }}
                >
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
          <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={snacks.openError}
            autoHideDuration={4000}
            onClose={() => setSnacks({ ...snacks, openError: false })}
          >
            <Alert
              onClose={() => setSnacks({ ...snacks, openError: false })}
              severity="error"
              sx={{ width: '100%' }}
            >
              {snacks.error}
            </Alert>
          </Snackbar>
          <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={snacks.openSuccess}
            autoHideDuration={4000}
            onClose={() => setSnacks({ ...snacks, openSuccess: false })}
          >
            <Alert
              onClose={() => setSnacks({ ...snacks, openSuccess: false })}
              severity="success"
              sx={{ width: '100%' }}
            >
              Registration completed!
            </Alert>
          </Snackbar>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
