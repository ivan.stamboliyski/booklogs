import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { Pagination, Typography, Snackbar, Alert } from '@mui/material';
import BookCard from '../components/BookCard/BookCard';
import { bookCardsPerPage } from '../common/constants';
import { getAllBooks, getBooksByField } from '../services/requests';
import AppContext from '../context/AppContext';
import { withRouter } from 'react-router';
import BookFilter from '../components/BookFilter/BookFilter';

const AllBooks = ({ history, match, setSortDirection, setSortField }) => {
  const {
    books,
    setBooks,
    keyword,
    filterField,
    sortField,
    sortDirection,
    page,
    setPage,
  } = useContext(AppContext);
  const [count, setCount] = useState(0);
  const [snack, setSnack] = useState({
    open: false,
    message: 'Error loading data! Try later!',
    severity: 'error',
  });

  const handleChange = (event, value) => {
    setPage(value);
    history.push(`/books/pages/${value}`);
  };

  useEffect(() => {
    if (keyword) {
      getBooksByField(
        setBooks,
        setCount,
        filterField,
        keyword,
        page,
        sortField,
        sortDirection,
        setSnack
      );
    } else {
      getAllBooks(setBooks, setCount, page, sortField, sortDirection, setSnack);
    }
  }, [keyword, setBooks, page, sortField, sortDirection]);

  useEffect(() => {
    history.push(`/books/pages/${match.params.number}`);
    setPage(+match.params.number);
  }, [match.params.number]);

  return (
    <Container sx={{ pb: 15, pt: 2 }} maxWidth="lg">
      {keyword && books.length === 0 && (
        <Typography variant="h4" sx={{ textAlign: 'center' }}>
          No results for <i>{keyword}</i>
        </Typography>
      )}

      <BookFilter
        setSortDirection={setSortDirection}
        setSortField={setSortField}
      />

      <Grid container spacing={[5, 8, 6]}>
        {books.map((book) => (
          <Grid item key={book.id} xs={6} sm={4} md={3}>
            <BookCard book={book} />
          </Grid>
        ))}
      </Grid>

      <Box sx={{ mt: 5, display: 'flex', justifyContent: 'center' }}>
        {books.length > 0 && (
          <Pagination
            count={Math.ceil(count / bookCardsPerPage)}
            page={page}
            onChange={handleChange}
          />
        )}
      </Box>

      {keyword && books.length > 0 && (
        <Typography variant="subtitle2" sx={{ textAlign: 'center' }}>
          Total results: <strong>{count}</strong>
        </Typography>
      )}
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={snack.open}
        autoHideDuration={6000}
        onClose={() => setSnack({ ...snack, message: '', open: false })}
      >
        <Alert
          onClose={() => setSnack({ ...snack, message: '', open: false })}
          severity={snack.severity}
          sx={{ width: '100%' }}
        >
          {snack.message}
        </Alert>
      </Snackbar>
    </Container>
  );
};

AllBooks.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  setSortDirection: PropTypes.func.isRequired,
  setSortField: PropTypes.func.isRequired,
};
export default withRouter(AllBooks);
