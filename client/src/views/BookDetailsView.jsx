import {
  Stack,
  Divider,
  Container,
  Box,
  Typography,
  Button,
  IconButton,
  Snackbar,
  Alert,
} from '@mui/material';
import PropTypes from 'prop-types';
import { useState, useEffect, useContext } from 'react';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { getBookById, getBookReviews } from './../services/requests.js';
import BookReview from '../components/BookReview/BookReview.jsx';
import DialogReview from '../components/DialogReview/DialogReview.jsx';
import CreateReview from './../components/CreateReview/CreateReview.jsx';
import CustomRating from '../components/CustomRating/CustomRating.jsx';
import AuthContext from '../context/AuthContext.js';
import { withRouter } from 'react-router';
import AppContext from '../context/AppContext.js';

const BookDetailsView = ({ match, history }) => {
  const { user } = useContext(AuthContext);
  const { bannedUser } = useContext(AppContext);
  const [book, setBook] = useState({
    id: null,
    userId: null,
    status: null,
    cover: null,
    title: '',
    author: '',
  });
  const [reviews, setBookReviews] = useState([]);
  const [expandedReviews, setExpandedReviews] = useState(true);
  const [open, setOpen] = useState(false);
  const [isModalOpen, setModalOpen] = useState(false);
  const [hasToUpdate, setHasToUpdate] = useState(false);
  const [deleteUserReview, setDeleteUserReview] = useState(false);
  const [dialogAction, setDialogAction] = useState('');
  const [initialValues, setInitialValues] = useState({
    title: '',
    content: '',
  });
  const [reviewForm, setReviewForm] = useState({});
  const [snack, setSnack] = useState({
    open: false,
    message: '',
    severity: 'error',
  });

  useEffect(() => {
    getBookById(match.params.id, setBook, setSnack);
  }, [match.params.id, book.status]);

  useEffect(() => {
    getBookReviews(book.id, setBookReviews, setSnack);
  }, [book, reviews.length]);

  useEffect(() => {
    setInitialValues({
      title: reviews.find((r) => r.userId === user.id)
        ? reviews.find((r) => r.userId === user.id).title
        : '',
      content: reviews.find((r) => r.userId === user.id)
        ? reviews.find((r) => r.userId === user.id).content
        : '',
    });
    setReviewForm({
      title: initialValues.title || '',
      content: initialValues.content || '',
    });
  }, [reviews]);

  const handleExpanded = () => {
    setExpandedReviews((isExpanded) => !isExpanded);
  };

  const openDialog = (event) => {
    if (event.currentTarget.getAttribute('aria-label') === 'delete') {
      setDeleteUserReview(true);
      setDialogAction('deleteReview');
    } else if (book.status === 'borrowed') {
      setDialogAction('returnBook');
    } else {
      setDialogAction('borrowBook');
    }

    setOpen(true);
  };

  const openModal = (event) => {
    if (
      reviews.find((r) => user.id === r.userId) &&
      event.currentTarget.getAttribute('aria-label') !== 'edit'
    ) {
      return setSnack({
        severity: 'error',
        message: 'You already wrote a review for this book!',
        open: true,
      });
    }

    if (event.currentTarget.getAttribute('aria-label') === 'edit') {
      setHasToUpdate(true);
    }

    setModalOpen(true);
  };

  return (
    <Container sx={{ pl: 10 }}>
      {book.id && (
        <>
          <IconButton
            onClick={() => history.goBack()}
            size="large"
            sx={{
              position: 'static',
              '&:hover': { bgcolor: '#3A6351', color: 'white' },
            }}
          >
            <ArrowBackIcon fontSize="inherit" />
          </IconButton>
          <Stack
            divider={<Divider orientation="vertical" flexItem />}
            direction={{ xs: 'column', sm: 'row' }}
            spacing={{ xs: 1, sm: 2, md: 4 }}
            height="100%"
          >
            <Box
              component="img"
              sx={{
                height: '100%',
                width: '500px',
                maxHeight: { xs: 300, md: 580 },
                maxWidth: { xs: 250, md: 500 },
              }}
              src={`http://localhost:5000/covers/${book.cover}`}
              alt="book cover"
            />
            <Container
              sx={{
                height: '100%',
                width: '500px',
                maxHeight: { xs: 500, md: 580 },
                maxWidth: { xs: 400, md: 650 },
              }}
            >
              <Typography variant="h4">{book.title}</Typography>
              <Typography variant="subtitle1">Author: {book.author}</Typography>
              <CustomRating
                book={book}
                bannedUser={bannedUser}
                setSnack={setSnack}
              />
              <br />
              <Button
                onClick={(event) => openDialog(event)}
                disabled={
                  (book.status === 'borrowed' && user.id !== book.userId) ||
                  bannedUser
                }
                size="small"
                sx={{
                  '&:hover': {
                    bgcolor: '#3A6351',
                    color: 'white',
                  },
                  color: '#3A6351',
                  '&:disabled': { color: '#8d2222' },
                  border: '1px solid black',
                  mt: 1,
                }}
              >
                {book.status === 'borrowed'
                  ? user.id !== book.userId
                    ? 'Borrowed'
                    : 'Return'
                  : 'Borrow'}
              </Button>
              <Button
                onClick={(event) => openModal(event)}
                value="Review"
                size="small"
                disabled={bannedUser}
                sx={{
                  ml: '8px',
                  '&:hover': {
                    bgcolor: '#3A6351',
                    color: 'white',
                  },
                  color: '#3A6351',
                  '&:disabled': { color: '#8d2222' },
                  border: '1px solid black',
                  mt: 1,
                }}
              >
                Review
              </Button>
              <CreateReview
                {...book}
                setBookReviews={setBookReviews}
                isModalOpen={isModalOpen}
                setModalOpen={setModalOpen}
                snack={snack}
                setSnack={setSnack}
                hasToUpdate={hasToUpdate}
                setHasToUpdate={setHasToUpdate}
                initialValues={initialValues}
                reviewForm={reviewForm}
                setReviewForm={setReviewForm}
              />
              <Container
                sx={{ mt: '30px', ml: -4, width: ['20rem', '28rem', '38rem'] }}
              >
                <Accordion expanded={expandedReviews} onChange={handleExpanded}>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography>Reviews</Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    {reviews.length > 0 ? (
                      reviews.map((r) => (
                        <BookReview
                          key={r.userId + r.bookId}
                          {...r}
                          bannedUser={bannedUser}
                          openModal={openModal}
                          openDialog={openDialog}
                          setSnack={setSnack}
                        />
                      ))
                    ) : (
                      <Typography>
                        There are no reviews for this book!
                      </Typography>
                    )}
                  </AccordionDetails>
                </Accordion>
              </Container>
            </Container>
          </Stack>
        </>
      )}
      <DialogReview
        open={open}
        setOpen={setOpen}
        book={book}
        setBook={setBook}
        setSnack={setSnack}
        setBookReviews={setBookReviews}
        deleteUserReview={deleteUserReview}
        setDeleteUserReview={setDeleteUserReview}
        dialogAction={dialogAction}
        setDialogAction={setDialogAction}
        initialValues={initialValues}
        reviewForm={reviewForm}
        setReviewForm={setReviewForm}
      />
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={snack.open}
        autoHideDuration={4000}
        onClose={() => setSnack({ ...snack, message: '', open: false })}
      >
        <Alert
          onClose={() => setSnack({ ...snack, message: '', open: false })}
          severity={snack.severity}
          sx={{ width: '100%' }}
        >
          {snack.message}
        </Alert>
      </Snackbar>
    </Container>
  );
};

BookDetailsView.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default withRouter(BookDetailsView);
