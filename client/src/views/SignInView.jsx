import * as React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Link } from 'react-router-dom';
import { routes } from './../common/constants.js';
import AuthContext, { getUser } from '../context/AuthContext.js';
import { withRouter } from 'react-router';
import { validateCredential } from '../common/helpers.js';
import { Alert, Snackbar } from '@mui/material';
import { updateForm } from '../common/helpers.js';

const theme = createTheme();
function SignIn({ history }) {
  const { setAuth } = React.useContext(AuthContext);
  const [userForm, setUserForm] = React.useState({
    username: '',
    password: '',
  });
  const [open, setOpen] = React.useState(false);
  const [error, setError] = React.useState('');
  const [showPassword, setShowPassword] = React.useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch('http://localhost:5000/api/v1/users/login', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userForm),
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.message) {
          throw new Error(data.message);
        }

        try {
          localStorage.setItem('library-token', data.token);
          localStorage.setItem('userActions', JSON.stringify({}));

          const user = getUser();
          setAuth({ user, isLoggedIn: true });
        } catch {
          throw new Error('Something went wrong!');
        }
      })
      .then(() => history.push('/'))
      .catch(({ message }) => {
        setError(message);
        setOpen(true);
      });
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: '#242423' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              error={!validateCredential(userForm.username)}
              helperText={
                !validateCredential(userForm.username) &&
                'Username must be between 3-25 characters or more'
              }
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              value={userForm.username}
              onChange={(e) => updateForm(setUserForm, 'username', e.target.value)}
              autoFocus
            />
            <TextField
              error={!validateCredential(userForm.password)}
              helperText={
                !validateCredential(userForm.password) &&
                'Password must be between 3-25 characters or more'
              }
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type={showPassword ? 'text' : 'password'}
              id="password"
              value={userForm.password}
              onChange={(e) => updateForm(setUserForm, 'password', e.target.value)}
              autoComplete="current-password"
            />
            <FormControlLabel
              onChange={() => setShowPassword(!showPassword)}
              sx={{ float: 'right' }}
              control={<Checkbox value="show-pass" color="primary" />}
              label="Show password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link
                  to={routes.signup}
                  variant="body2"
                  style={{ color: '#1976d2', fontSize: '0.875rem' }}
                >
                  "Don't have an account? Sign Up"
                </Link>
              </Grid>
            </Grid>
          </Box>
          <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={open}
            autoHideDuration={4000}
            onClose={() => setOpen(false)}
          >
            <Alert
              onClose={() => setOpen(false)}
              severity="error"
              sx={{ width: '100%' }}
            >
              {error}
            </Alert>
          </Snackbar>
        </Box>
      </Container>
    </ThemeProvider>
  );
}

SignIn.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(SignIn);
