import { Container, Stack, Typography } from '@mui/material';
import Carousel from '../components/Carousel/Carousel';
import PublicHeader from '../components/Header/Header-public';

const PublicHomeView = () => {
  return (
    <>
      <PublicHeader />
      <Container>
        <Stack
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={6}
        >
          <Typography
            variant="h2"
            gutterBottom
            component="div"
            sx={{ fontSize: ['2em', '2.2em', '2.5em'], mt: 3, mb: 4 }}
          >
            WELCOME TO BOOKLOGS
          </Typography>
          <Carousel />
        </Stack>
      </Container>
    </>
  );
};

export default PublicHomeView;
