import { createContext } from 'react';
import jwt from 'jsonwebtoken';

const AuthContext = createContext({
  user: {
    id: 0,
    username: '',
    role: '',
  },
  isLoggedIn: false,
  setAuth: () => { },
});

export const getToken = () => localStorage.getItem('library-token') || '';

export const getUser = () => {
  try {
    const payload = jwt.decode(getToken());

    return { id: payload.sub, username: payload.username, role: payload.role };
  } catch {
    return null;
  }
};

export default AuthContext;
