import { createContext } from 'react';

const AppContext = createContext({
  books: [],
  setBooks() { },
  keyword: '',
  filterField: '',
  page: 1,
  userDetails: {
    id: 0,
    username: '',
    role: '',
    avatar: '',
    points: 0,
    banstatus: {},
  },
  sortField: '',
  sortDirection: '',
  bannedUser: false,
});

export default AppContext;
