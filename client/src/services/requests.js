import { API, bookCardsPerPage } from '../common/constants';
import { getToken } from '../context/AuthContext';
import { setUserActionsRecords } from '../common/helpers.js';

export const postBorrow = (book, setBook, setSnack) => {
  fetch(`${API}/books/${book.id}`, {
    method: 'POST',
    headers: {
      authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((r) => {
      setBook((book) => ({ ...book, status: 'borrowed' }));
      setSnack({
        message: `You have borrowed ${book.title}!`,
        severity: 'success',
        open: true,
      });
      setUserActionsRecords(r.id, 'borrowedBook');
    })
    .catch((e) =>
      setSnack({
        message: `An error came up! Try Later!`,
        severity: 'error',
        open: true,
      })
    );
};

export const returnBook = (book, setBook, setSnack) => {
  fetch(`${API}/books/${book.id}`, {
    method: 'DELETE',
    headers: {
      authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((r) => {
      setBook((book) => ({ ...book, status: 'available' }));
      setSnack({
        message: `You have returned ${book.title}!`,
        severity: 'success',
        open: true,
      });
      setUserActionsRecords(r.id, 'returnedBook');
    })
    .catch((e) =>
      setSnack({
        message: `An error came up! Try Later!`,
        severity: 'error',
        open: true,
      })
    );
};

export const getAllBooks = (
  setBooks,
  setCount,
  page,
  sortField,
  sortDirection,
  setSnack
) => {
  fetch(`${API}/books/?count=100`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((data) => {
      setCount(data.pagination.count);
    })
    .catch(() => setSnack((snack) => ({ ...snack, open: true })));

  fetch(
    `${API}/books?offset=${(page - 1) * bookCardsPerPage
    }&count=${bookCardsPerPage}&order_by=${sortField}&order_dir=${sortDirection}`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    }
  )
    .then((r) => r.json())
    .then((data) => {
      setBooks(data.books);
    })
    .catch(() => setSnack((snack) => ({ ...snack, open: true })));
};

export const getBooksByField = (
  setBooks,
  setCount,
  field,
  searchValue,
  page,
  sortField,
  sortDirection,
  setSnack
) => {
  fetch(`${API}/books?count=100&${field}=${searchValue}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((data) => {
      setCount(data.pagination.count);
    })
    .catch(() => setSnack((snack) => ({ ...snack, open: true })));

  fetch(
    `${API}/books?${field}=${searchValue}&offset=${(page - 1) * bookCardsPerPage
    }&count=${bookCardsPerPage}&order_by=${sortField}&order_dir=${sortDirection}`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    }
  )
    .then((r) => r.json())
    .then((data) => {
      setBooks(data.books);
    })
    .catch(() => setSnack((snack) => ({ ...snack, open: true })));
};

export const getBookById = (id, setBook, setSnack) => {
  fetch(`${API}books/${id}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((book) => setBook(book))
    .catch((error) => {
      setSnack({
        severity: 'error',
        message: error.message,
        open: true,
      });
    });
};

export const getBookReviews = (id, setReviews, setSnack) => {
  fetch(`${API}books/${id}/reviews`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((reviews) => setReviews(reviews))
    .catch((error) => {
      setSnack({
        severity: 'error',
        message: error.message,
        open: true,
      });
    });
};

export const getUserInfo = (id, setUserDetails) => {
  fetch(`${API}users/${id}`, {
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then(setUserDetails)
    .catch(console.error);
};

export const postReview = (
  id,
  review,
  setSnack,
  setBookReviews,
  setReviewForm
) => {
  fetch(`${API}books/${id}/reviews`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      title: review.title,
      content: review.content,
    }),
  })
    .then((r) => r.json())
    .then((data) => {
      try {
        setBookReviews((reviews) => [...reviews, data]);
        setReviewForm({
          title: data.title,
          content: data.content,
        });
        setSnack({
          message: 'Your review has been added!',
          severity: 'success',
          open: true,
        });

        setUserActionsRecords(data.bookId, 'wroteReview');
      } catch (error) {
        throw new Error(error);
      }
    })
    .catch((error) => {
      setSnack({
        severity: 'error',
        message: error.message,
        open: true,
      });
    });
};

export const updateReview = (id, body, setSnack, setBookReviews) => {
  fetch(`${API}books/${id}/reviews`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then((r) => r.json())
    .then(() => {
      setBookReviews([]);
      setSnack({
        message: 'Your review has been updated!',
        severity: 'success',
        open: true,
      });
    })
    .catch(({ errorMsg }) => {
      setSnack({ severity: 'error', message: errorMsg, open: true });
    });
};

export const deleteReview = (id, setSnack, setBookReviews) => {
  fetch(`${API}books/${id}/reviews`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
  })
    .then((r) => r.json())
    .then((data) => {
      setSnack({
        message: 'Your review has been deleted!',
        severity: 'success',
        open: true,
      });
      setBookReviews((reviews) => [
        ...reviews.filter((r) => r.userId !== data.userId),
      ]);
    })
    .catch(({ errorMsg }) => {
      setSnack({ severity: 'error', message: errorMsg, open: true });
    });
};

export const rateBook = (id, value, setRating, setSnack) => {
  fetch(`${API}books/${id}/rate`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ value }),
  })
    .then((r) => r.json())
    .then((data) => {
      setRating(data.rating);
      setSnack({
        message: `You rated ${data.title}!`,
        severity: 'success',
        open: true,
      });
    })
    .catch((e) => {
      setSnack({ severity: 'error', message: e.message, open: true });
    });
};

export const adminGetAllUsers = (setUsers, setSnack) => {
  fetch(`${API}admin/users`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((data) => {
      setUsers(data);
    })
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminDeleteUser = (id, setCurrentUser, setSnack) => {
  fetch(`${API}admin/users/${id}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then(() => setCurrentUser({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminUnbanUser = (id, setCurrentUser, setSnack) => {
  fetch(`${API}admin/users/${id}/ban`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      description: 'Unbanned',
      expiration: 1638223200000,
      banned: 0,
    }),
  })
    .then((r) => r.json())
    .then((r) => setCurrentUser({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminBanUser = (id, body, setCurrentUser, setSnack) => {
  fetch(`${API}admin/users/${id}/ban`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then((r) => r.json())
    .then((r) => setCurrentUser({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminUpdateUser = (id, form, setCurrentUser, setSnack) => {
  fetch(`${API}admin/users/${id}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
    body: form,
  })
    .then((r) => r.json())
    .then(() => {
      setCurrentUser({});
    })
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminGetAllBooks = (setBooks, setSnack) => {
  fetch(`${API}admin/books/?count=100`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((data) => {
      setBooks(data.books);
    })
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminDeleteBook = (id, setCurrentBook, setSnack) => {
  fetch(`${API}admin/books/${id}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then(() => setCurrentBook({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminUpdateBook = (id, form, setCurrentBook, setSnack) => {
  fetch(`${API}admin/books/${id}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
    body: form,
  })
    .then((r) => r.json())
    .then(() => setCurrentBook({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminCreateBook = (form, setCurrentBook, setSnack) => {
  fetch(`${API}admin/books/`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
    body: form,
  })
    .then((r) => r.json())
    .then(() => setCurrentBook({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminGetAllReviews = (setReviews, setSnack) => {
  fetch(`${API}admin/reviews/?count=100`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then(setReviews)
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminUpdateReview = (
  bookId,
  userId,
  form,
  setCurrentReview,
  setSnack
) => {
  fetch(`${API}admin/books/${bookId}/reviews/${userId}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(form),
  })
    .then((r) => r.json())
    .then(() => setCurrentReview({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const adminDeleteReview = (
  bookId,
  userId,
  setCurrentReview,
  setSnack
) => {
  fetch(`${API}admin/books/${bookId}/reviews/${userId}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then(() => setCurrentReview({}))
    .catch((e) =>
      setSnack((snack) => ({ ...snack, open: true, message: e.message }))
    );
};

export const likeReview = (id, reviewerId, setLikesCount, setSnack) => {
  fetch(`${API}books/${id}/reviews/${reviewerId}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${getToken()}`,
      'Content-Type': 'application/json',
    },
  })
    .then((r) => r.json())
    .then((data) => {
      setLikesCount(data.votes.length);
    })
    .catch(({ message }) => {
      setSnack({ severity: 'error', message: message, open: true });
    });
};
