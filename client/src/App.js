import './App.css';
import PrivateHeader from './components/Header/Header-private';
import PublicHeader from './components/Header/Header-public';
import SignIn from './views/SignInView.jsx';
import SignUp from './views/SignUpView.jsx';
import PublicHomeView from './views/PublicHomeView';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { routes } from './common/constants';
import AllBooks from './views/AllBooksView';
import { useEffect, useState } from 'react';
import AuthContext, { getUser } from './context/AuthContext';
import PrivateRoute from './components/hoc/PrivateRoute';
import BookDetailsView from './views/BookDetailsView';
import AppContext from './context/AppContext';
import { getUserInfo } from './services/requests';
import AdminView from './views/AdminView';
import { Page404 } from './views/Page404';
import { Container, Typography } from '@mui/material';
function App() {
  const [userDetails, setUserDetails] = useState({ avatar: null, banstatus: null });
  const [{ isLoggedIn, user }, setAuth] = useState({
    isLoggedIn: !!getUser(),
    user: getUser(),
  });
  const [books, setBooks] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [filterField, setFilterField] = useState('id');
  const [sortField, setSortField] = useState('id');
  const [sortDirection, setSortDirection] = useState('ASC');
  const [page, setPage] = useState(1);
  const [bannedUser, setBannedUser] = useState(false);

  useEffect(() => {
    if (isLoggedIn) {
      getUserInfo(user.id, setUserDetails);
    }
  }, [setAuth]);

  useEffect(() => {
    setBannedUser(
      userDetails.banstatus !== null ||
      userDetails.banstatus?.banned === 1
    );
  }, [userDetails]);

  return (
    <AuthContext.Provider value={{ isLoggedIn, user, setAuth }}>
      <AppContext.Provider
        value={{
          books,
          setBooks,
          keyword,
          filterField,
          sortField,
          sortDirection,
          page,
          setPage,
          userDetails,
          bannedUser,
        }}
      >
        <BrowserRouter>
          <div className="App">
            {isLoggedIn ? (
              <PrivateHeader
                setKeyword={setKeyword}
                setFilterField={setFilterField}
                setUserDetails={setUserDetails}
                setBannedUser={setBannedUser}
              />
            ) : (
              <PublicHeader />
            )}
            {bannedUser &&
              (
                <Container sx={{ bgcolor: 'red', width: '30%', border: '3px solid red' }}>
                  <Typography sx={{ textAlign: 'center' }}>
                    You are banned till {new Date(userDetails.banstatus.expiration).toLocaleDateString()}!
                  </Typography>
                </Container>
              )}
            <Switch>
              <Redirect
                from="/"
                exact
                to={isLoggedIn ? `${routes.books}/pages/1` : routes.home}
              />
              <Route path={routes.home} exact component={PublicHomeView} />
              <Route path={routes.login} exact component={SignIn} />
              <Route path={routes.signup} component={SignUp} />
              <PrivateRoute
                exact
                auth={isLoggedIn}
                path={`${routes.books}/:id`}
                component={BookDetailsView}
              />
              <PrivateRoute
                exact
                auth={isLoggedIn}
                path={`${routes.books}/pages/:number`}
                component={AllBooks}
                props={{ setSortField, setSortDirection }}
              />
              <PrivateRoute
                exact
                auth={isLoggedIn}
                path="/admin"
                component={AdminView}
              />
              <Route path="*" component={Page404} />
            </Switch>
          </div>
        </BrowserRouter>
      </AppContext.Provider>
    </AuthContext.Provider>
  );
}

export default App;
