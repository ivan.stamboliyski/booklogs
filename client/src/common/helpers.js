import {
  maxCredentialLength,
  minCredentialLength,
  reviewInputs,
} from './constants';

/**
 * Checks if credential is valid
 * @param {string} value
 * @returns {boolean}
 */
export const validateCredential = (value) => {
  if (typeof value !== 'string') return false;

  return (
    value.length === 0 ||
    (value.length >= minCredentialLength && value.length <= maxCredentialLength)
  );
};

export const validateReviewInputs = (value, prop) => {
  if (typeof value !== 'string') return false;

  return (
    value.length === 0 ||
    (value.length >= reviewInputs[prop].minLength &&
      value.length <= reviewInputs[prop].maxLength)
  );
};

export const updateForm = (setForm, prop, value) => {
  setForm((prevState) => ({
    ...prevState,
    [prop]: value,
  }));
};

export const setUserActionsRecords = (id, action) => {

  let actionsRecords = JSON.parse(localStorage.getItem('userActions'));

  if (actionsRecords === null || !actionsRecords.hasOwnProperty(id)) {
    actionsRecords = JSON.parse(localStorage.getItem('userActions'));
    actionsRecords = {
      ...actionsRecords,
      [id]: {
        borrowedBook: false,
        returnedBook: false,
        wroteReview: false
      }
    };
  }

  actionsRecords[id][action] = true;

  localStorage.setItem('userActions', JSON.stringify(actionsRecords));
};

export const canRate = (id) => {
  return JSON.parse(localStorage.getItem('userActions')).hasOwnProperty(id) ?
    Object.values(JSON.parse(localStorage.getItem('userActions'))[id]).every(x => x === true) :
    false;
};
