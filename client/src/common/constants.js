export const routes = {
  login: '/login',
  signup: '/signup',
  home: '/home',
  books: '/books',
};

export const API = 'http://localhost:5000/api/v1/';
export const minCredentialLength = 3;
export const maxCredentialLength = 25;
export const bookCardsPerPage = 8;
export const reviewInputs = {
  reviewTitle: {
    minLength: 5,
    maxLength: 50,
  },
  reviewContent: {
    minLength: 10,
    maxLength: 500
  }
};
export const dialogMessages = {
  title: {
    deleteReview: 'Delete review?',
    borrowBook: 'Borrow a book?',
    returnBook: 'Return a book?'
  },
  content: {
    deleteReview: 'Are you sure you want to delete your review for ',
    borrowBook: `Would you like to proceed with borrowing `,
    returnBook: `Would you like to proceed with returning `
  }
};
